//
//  NSObject+Hooking.m
//  FastMenu
//
//  Created by Milen Halachev on 11/5/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import "NSObject+Hooking.h"
#import <objc/runtime.h>

@implementation NSObject (Hooking)

+(void)hookSourceClass:(Class)sourceCls sourceMethod:(SEL)sourceMethod destinationClass:(Class)destinationClass destinationMethod:(SEL)destinationMethod
{
    Method original = class_getInstanceMethod(sourceCls, sourceMethod);
    
    IMP imp = class_getMethodImplementation(destinationClass, destinationMethod);
    const char *types = method_getTypeEncoding(original);
    if (class_addMethod(sourceCls, destinationMethod, imp, types)) 
    {
        
    }
    
    Method test = class_getInstanceMethod(sourceCls, destinationMethod);
    
    method_exchangeImplementations(original, test);
    
}

//+(void)hookSourceClass:(Class)sourceCls sourceMethod:(SEL)sourceMethod block:(id)block
//{
//    IMP blockImplementation = imp_implementationWithBlock(block);
//    NSString *sourceMethodName = NSStringFromSelector(sourceMethod);
//    NSString *blockMethodName = [NSString stringWithFormat:@"hook_block_%@", sourceMethodName];
//    SEL blockSelector = NSSelectorFromString(blockMethodName);
//
//    
//    
//    Method sourceM = class_getInstanceMethod(sourceCls, sourceMethod);
//    if (class_addMethod(sourceCls, blockSelector, blockImplementation, method_getTypeEncoding(sourceM))) 
//    {
//        
//    }
//    
//    Method blockM = class_getInstanceMethod(sourceCls, blockSelector);
//    
//    method_exchangeImplementations(sourceM, blockM);
//    
//    
////    imp_removeBlock - for release of the newImplementation
//    
////    Method method = class_getInstanceMethod(sourceCls, sourceMethod);
//////    class_replaceMethod(sourceCls, sourceMethod, newImplementation, method_getTypeEncoding(method));
////    class_addMethod(sourceCls, sourceMethod, blockImplementation, method_getTypeEncoding(method));
//}
    
@end
