//
//  UITextField+InputValidation.h
//  MHCategories
//
//  Created by Milen Halachev on 5/30/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef BOOL(^NSObjectInputValidationBlock)(id obj);

@interface NSObject (InputValidation)

-(void)addInputValidationBlock:(NSObjectInputValidationBlock)block;
-(BOOL)validateInput;

@end
