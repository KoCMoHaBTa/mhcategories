//
//  UIImage+UIColor.h
//  Pizza
//
//  Created by Milen Halachev on 3/20/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIColor)

//+ (UIImage *) imageFromColor:(UIColor *)color frame:(CGRect)aFrame;

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
