//
//  NSString+Validation.h
//  PhoneRace
//
//  Created by Milen Halachev on 10/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


//extern NSString* const EMPTY_STRING;


@interface NSString (NSString_Validation)

-(BOOL)isValidEmailAddressUsingStricterFilter:(BOOL)strict;
-(BOOL)isEqualToEmptyString;
-(NSString*)firstChar;

@end
