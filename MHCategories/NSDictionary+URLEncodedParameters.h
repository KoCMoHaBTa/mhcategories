//
//  NSDictionary+URLEncodedParameters.h
//  MHCategories
//
//  Created by Milen Halachev on 5/30/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (URLEncodedParameters)

-(NSString*)URLEncodedParameters;

@end
