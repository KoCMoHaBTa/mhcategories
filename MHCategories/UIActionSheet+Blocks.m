//
//  UIActionSheet+Blocks.m
//  MHCategories
//
//  Created by Milen Halachev on 3/7/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "UIActionSheet+Blocks.h"
#import <objc/runtime.h>

@implementation UIActionSheet (Blocks)

@dynamic buttonClickedBlock;
@dynamic cancelBlock;
@dynamic willPresentBlock;
@dynamic didPresentBlock;
@dynamic willDismissBlock;
@dynamic didDismissBlock;

static char buttonClickedBlockKey;
static char cancelBlockKey;
static char willPresentBlockKey;
static char didPresentBlockKey;
static char willDismissBlockKey;
static char didDismissBlockKey ;

-(UIActionSheetButtonClickedBlock)buttonClickedBlock
{
    return objc_getAssociatedObject(self, &buttonClickedBlockKey);
}

-(void)setButtonClickedBlock:(UIActionSheetButtonClickedBlock)buttonClickedBlock
{
    objc_setAssociatedObject(self, &buttonClickedBlockKey, buttonClickedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIActionSheetCancelBlock)cancelBlock
{
    return objc_getAssociatedObject(self, &cancelBlockKey);
}

-(void)setCancelBlock:(UIActionSheetCancelBlock)cancelBlock
{
    objc_setAssociatedObject(self, &cancelBlockKey, cancelBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIActionSheetWillPresentBlock)willPresentBlock
{
    return objc_getAssociatedObject(self, &willPresentBlockKey);
}

-(void)setWillPresentBlock:(UIActionSheetWillPresentBlock)willPresentBlock
{
    objc_setAssociatedObject(self, &willPresentBlockKey, willPresentBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIActionSheetDidPresentBlock)didPresentBlock
{
    return objc_getAssociatedObject(self, &didPresentBlockKey);
}

-(void)setDidPresentBlock:(UIActionSheetDidPresentBlock)didPresentBlock
{
    objc_setAssociatedObject(self, &didPresentBlockKey, didPresentBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIActionSheetWillDismissBlock)willDismissBlock
{
    return objc_getAssociatedObject(self, &willDismissBlockKey);
}

-(void)setWillDismissBlock:(UIActionSheetWillDismissBlock)willDismissBlock
{
    objc_setAssociatedObject(self, &willDismissBlockKey, willDismissBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIActionSheetDidDismissBlock)didDismissBlock
{
    return objc_getAssociatedObject(self, &didDismissBlockKey);
}

-(void)setDidDismissBlock:(UIActionSheetDidDismissBlock)didDismissBlock
{
    objc_setAssociatedObject(self, &didDismissBlockKey, didDismissBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.buttonClickedBlock) 
    {
        self.buttonClickedBlock(actionSheet, buttonIndex);
    }
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    if (self.cancelBlock) 
    {
        self.cancelBlock(actionSheet);
    }
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    if (self.willPresentBlock) 
    {
        self.willPresentBlock(actionSheet);
    }
}

- (void)didPresentActionSheet:(UIActionSheet *)actionSheet
{
    if (self.didPresentBlock) 
    {
        self.didPresentBlock(actionSheet);
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.willDismissBlock) 
    {
        self.willDismissBlock(actionSheet, buttonIndex);
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.didDismissBlock) 
    {
        self.didDismissBlock(actionSheet, buttonIndex);
    }
}


#pragma mark - Block Constructors

+(UIActionSheet *)actionSheetWithTitle:(NSString *)title dismissBlock:(UIActionSheetDidDismissBlock)dismissBlock cancelButtonTitle:(NSString *)cancelButtonTitle destructiveButtonTitle:(NSString *)destructiveButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{   
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title 
                                                             delegate:nil 
                                                    cancelButtonTitle:nil 
                                               destructiveButtonTitle:nil 
                                                    otherButtonTitles:nil];
    
    actionSheet.delegate = (id<UIActionSheetDelegate>)actionSheet;
    [actionSheet setDidDismissBlock:dismissBlock];
    
    if (destructiveButtonTitle) 
    {
        [actionSheet addButtonWithTitle:destructiveButtonTitle];
        [actionSheet setDestructiveButtonIndex:0];
    }
    
    NSString *firstOtherButtonTitle = otherButtonTitles;
    NSString *anotherButtonTitle = nil;
    
    if (firstOtherButtonTitle)
    {
        [actionSheet addButtonWithTitle:firstOtherButtonTitle];
        
        va_list arguments;
        va_start(arguments, otherButtonTitles);
        
        while ((anotherButtonTitle = va_arg(arguments, id)))
        {
            [actionSheet addButtonWithTitle:anotherButtonTitle];
        }
        
        va_end(arguments);
    }
    
    if (cancelButtonTitle) 
    {
        [actionSheet addButtonWithTitle:cancelButtonTitle];
        [actionSheet setCancelButtonIndex:actionSheet.numberOfButtons-1];
    }

    
    return actionSheet;
}


@end
