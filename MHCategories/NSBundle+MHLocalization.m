//
//  NSBundle+MHLocalization.m
//  MHCategories
//
//  Created by Milen Halachev on 6/7/13.
//  Copyright (c) 2013 Milen Halachev. All rights reserved.
//

#import "NSBundle+MHLocalization.h"
#import <objc/runtime.h>

#define MHLocalizationTableKey @"MHLocalizationTableKey"
#define MHPrevousSystemLanguageKey @"MHPrevousSystemLanguageKey"
#define MHPrevousSystemLocaleKey @"MHPrevousSystemLocaleKey"

@implementation NSBundle (MHLocalization)

static NSString *__currentLocalization = nil;

+(NSString *)currentLocalization
{
    return __currentLocalization;
}

+(void)setCurrentLocalization:(NSString *)currentLocalization
{
    //copy the current language to be used as prevous after change
    NSString *prevousLocalization = [__currentLocalization copy];
    
    //post language will change notification
    NSDictionary *userInfoLanguageWillChange = [self notificationUserInfoDictionaryForPrevousLanguage:__currentLocalization currentLanguage:__currentLocalization newLanguage:currentLocalization];
    [self postNotificationWithName:MHLocalizableLanguageWillChangeNotification userInfo:userInfoLanguageWillChange postingStyle:NSPostNow];
    
    //change the language
    __currentLocalization = [currentLocalization copy];
    [self saveLocalization:__currentLocalization];
    
//    MHLocalizationTable *newTable = [MHLocalizationTable tableForLocale:locale];
//    
//    if (newTable)
//    {
//        self.currentTable = newTable;
//    }
//    else
//    {
//        NSLog(@"Unsupported Language! Attempting to rollback.");
//        [self loadTable];
//    }
    
    //post language did chage notification
    NSDictionary *userInfoLanguageDidChange = [self notificationUserInfoDictionaryForPrevousLanguage:prevousLocalization currentLanguage:__currentLocalization newLanguage:__currentLocalization];
    [self postNotificationWithName:MHLocalizableLanguageDidChangeNotification userInfo:userInfoLanguageDidChange postingStyle:NSPostNow];
}

-(NSBundle *)bundleForCurrentLocalization
{
    return [self bundleForLocalization:[[self class] currentLocalization]];
}

-(NSBundle *)bundleForLocalization:(NSString *)localization
{
    if (!localization || [localization isEqualToString:@""])
    {
        return self;
    }
    
    localization = [localization stringByReplacingOccurrencesOfString:@"_" withString:@"-"];
    
    NSBundle *localizationBundle = [NSBundle bundleWithPath:[self pathForResource:localization ofType:@"lproj"]];
    
    NSRange dashRange = [localization rangeOfString:@"-"];
    if (!localizationBundle && dashRange.location != NSNotFound) 
    {
        localization = [localization substringToIndex:dashRange.location];
        return [self bundleForLocalization:localization];
    }
    
    return localizationBundle?:self;
}

#pragma mark - Method Swizzling

+(void)load
{
    Method lo = class_getInstanceMethod([self class], @selector(localizedStringForKey:value:table:));
    Method ll = class_getInstanceMethod([self class], @selector(loc_localizedStringForKey:value:table:));
    
    method_exchangeImplementations(lo, ll);
    
    
    [self loadLocalization];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActiveHandler:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(NSString *)loc_localizedStringForKey:(NSString *)key value:(NSString *)value table:(NSString *)tableName
{
    NSBundle *bundleForCurrentLocalization = [self bundleForCurrentLocalization];
    
    return [bundleForCurrentLocalization loc_localizedStringForKey:key value:value table:tableName];
}

#pragma mark - Localizable objects registration & deregistration

+(void)registerLocalizableObject:(id<MHLocalizable>)localizableObject
{
    [[NSNotificationCenter defaultCenter] addObserver:localizableObject selector:@selector(loadTranslations:) name:MHLocalizableLoadTranslationsNotification object:self];
    
    if ([localizableObject respondsToSelector:@selector(languageWillChange:)])
    {
        [[NSNotificationCenter defaultCenter] addObserver:localizableObject selector:@selector(languageWillChange:) name:MHLocalizableLanguageWillChangeNotification object:self];
    }
    
    if ([localizableObject respondsToSelector:@selector(languageDidChange:)])
    {
        [[NSNotificationCenter defaultCenter] addObserver:localizableObject selector:@selector(languageDidChange:) name:MHLocalizableLanguageDidChangeNotification object:self];
    }
}

+(void)deregisterLocalizableObject:(id<MHLocalizable>)localizableObject
{
    [[NSNotificationCenter defaultCenter] removeObserver:localizableObject name:MHLocalizableLoadTranslationsNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:localizableObject name:MHLocalizableLanguageWillChangeNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:localizableObject name:MHLocalizableLanguageDidChangeNotification object:self];
}

#pragma mark - Languages Information

+(NSString *)systemLocalization
{
    return [[self supportedSystemLocalizations] firstObject] ?: @"en";
}

+(NSString *)systemLocale
{
    return [[NSLocale currentLocale] localeIdentifier];
}

+(NSArray *)supportedSystemLocalizations
{
    return [NSLocale preferredLanguages];
}

+(NSArray *)supportedSystemLocales
{
    return [NSLocale availableLocaleIdentifiers];
}

+(NSString *)displayNameForLocalization:(NSString *)inputLocalization translatedToLocalization:(NSString *)traslationLocalization
{
    NSLocale *locale = nil;
    locale = [[NSLocale alloc] initWithLocaleIdentifier:traslationLocalization];
    
    NSString *displayName = [locale displayNameForKey:NSLocaleIdentifier value:inputLocalization];
    
    return displayName;
}

#pragma mark - NSNotification Management

+(void)postNotificationWithName:(NSString *)name userInfo:(NSDictionary *)userInfo postingStyle:(NSPostingStyle)postingStyle
{
    NSNotification *notification = [NSNotification notificationWithName:name object:self userInfo:userInfo];
    [[NSNotificationQueue defaultQueue] enqueueNotification:notification postingStyle:postingStyle];
}

+(NSDictionary *)notificationUserInfoDictionaryForPrevousLanguage:(NSString *)prevousLanguage currentLanguage:(NSString *)currentLanguage newLanguage:(NSString *)newLanguage
{
    return [NSDictionary dictionaryWithObjectsAndKeys:prevousLanguage, MHLocalizableNotificationUserInfoDictionaryPrevousLanguageKey,
            currentLanguage, MHLocalizableNotificationUserInfoDictionaryCurrentLanguageKey,
            newLanguage, MHLocalizableNotificationUserInfoDictionaryNewLanguageKey,
            nil];
}

#pragma mark - Localization Loading

+(void)loadLocalization
{
//    NSString *savedLocalization = [[NSUserDefaults standardUserDefaults] objectForKey:MHLocalizationTableKey];
//    
//    [self setCurrentLocalization:savedLocalization];

    
    
//    [self setCurrentLocalization:[self systemLocale]];
//    
//#warning TODO: finish Localization loading and specifically on first time app starts
    
    
    
    
//    //check if the system language has changed - if not: try to load saved table
//    if ([self systemLanguageHasChanged] == NO)
//    {
//        //create the default pattern
//        pattern = MHTableLoadingPatternCreateWithType(TableLoadingPatternTypeDefault, &count);
//    }
//    else
//    {
//        //save the current system language as prevous in user defaults
//        [[NSUserDefaults standardUserDefaults] setObject:[self systemLocalization] forKey:PrevousSystemLanguageKey];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        //create the system language changed pattern
//        pattern = MHTableLoadingPatternCreateWithType(TableLoadingPatternTypeSystemLanguageChanged, &count);
//    }
//    
//    
//    //saved
//    [self setCurrentLocalization:currentLocalization];
//    
//    //default = nil
//    //system language
//    //system locale
    
    
    
    
    /*
     
     1. if system language or locale has changed - remove saved localization
     2. if there is saved localization - use it.
     3. else set the system locale as current
     
     4. upon loading a translation, a bundle for {current_localization}.lproj is created
     5. if this folder does not exist - use the current bundle - i.e. main bundle
     6. load the translation from the bundle
     7. if this is current bundle, the system language will be used
     
     */
    
#warning add support for finer loading by system language or locale
#warning what if we have multiple bg.lproj - which one will be loaded?
    
    
    //if system language or locate has changed - reset
    BOOL systemLanguageHasChanged = [self systemLanguageHasChanged];
    BOOL systemLocaleHasChanged = [self systemLocaleHasChanged];
    
    if (systemLanguageHasChanged || systemLocaleHasChanged) 
    {
        //remove saved localization
        [self saveLocalization:nil];
    }
    
    //get some stuffs
//    NSString *systemLanguage = [self systemLocalization];
    NSString *systemLocale = [self systemLocale];
    NSString *savedLocalization = [self savedLocalization];
    
    if (savedLocalization) 
    {
        [self setCurrentLocalization:savedLocalization];
    }
    else
    {
        [self setCurrentLocalization:systemLocale];
    }
}

+(void)saveLocalization:(NSString*)localization
{    
    [[NSUserDefaults standardUserDefaults] setObject:localization forKey:MHLocalizationTableKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString*)savedLocalization
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:MHLocalizationTableKey];
}

+(BOOL)systemLanguageHasChanged
{
    //get the current system language
    NSString *currentSystemLanguage = [self systemLocalization];
    
    //register user defaults
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithObject:currentSystemLanguage forKey:MHPrevousSystemLanguageKey]];
    
    //load the prevous system language
    NSString *prevousLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:MHPrevousSystemLanguageKey];
    
    //save the current system language
    [[NSUserDefaults standardUserDefaults] setObject:currentSystemLanguage forKey:MHPrevousSystemLanguageKey];
    [[NSUserDefaults standardUserDefaults] synchronize];  
    
    return ![prevousLanguage isEqualToString:currentSystemLanguage];
}

+(BOOL)systemLocaleHasChanged
{
    //get the current system language
    NSString *currentSystemLocale = [self systemLocale];
    
    //register user defaults
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithObject:currentSystemLocale forKey:MHPrevousSystemLocaleKey]];
    
    //load the prevous system language
    NSString *prevousLocale = [[NSUserDefaults standardUserDefaults] objectForKey:MHPrevousSystemLocaleKey];
    
    //save the current system language
    [[NSUserDefaults standardUserDefaults] setObject:currentSystemLocale forKey:MHPrevousSystemLocaleKey];
    [[NSUserDefaults standardUserDefaults] synchronize];  
    
    return ![prevousLocale isEqualToString:currentSystemLocale];
}

+(void)applicationDidBecomeActiveHandler:(NSNotification *)notification
{
    [self loadLocalization];
}





//-(NSArray *)supportedNonBundleLanguages
//{
//    //load predefined languages
//#if UsePredefinedNonBundleLanguages
//    
//    NSArray *predefinedNonBundleLanguages = [NSArray arrayWithContentsOfFile:[DefaultBundle pathForResource:PredefinedNonBundleLanguagesFileName ofType:@""]];
//    return predefinedNonBundleLanguages;
//    
//#endif
//    
//    //scan for non bundle language tables
//#if ScanForNonBundleLangueTables
//    
//    NSMutableArray *nonBundleLanguages = [NSMutableArray array];
//    
//    NSArray *allStringsFiles = [DefaultBundle pathsForResourcesOfType:@"strings" inDirectory:nil];
//    
//    for (NSString *path in allStringsFiles)
//    {
//        NSString *language = [[path stringByDeletingPathExtension] lastPathComponent];
//        
//        if ([self.supportedSystemLocales containsObject:language])
//        {
//            [nonBundleLanguages addObject:language];
//        }
//    }
//    
//    return nonBundleLanguages;
//    
//#endif
//    
//    
//    //if non of above options is available
//    return nil;
//}
//






@end
