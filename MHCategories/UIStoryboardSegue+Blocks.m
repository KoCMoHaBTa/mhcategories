//
//  UIStoryboardSegue+Blocks.m
//  MHCategories
//
//  Created by Milen Halachev on 8/12/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "UIStoryboardSegue+Blocks.h"

@implementation UIStoryboardSegue (Blocks)

@end




@interface UIViewController (UIStoryboardSegueBlocksPrivate)

@property(nonatomic, retain) NSMutableDictionary *seguePerformBlocksDictionary;

@end

@implementation UIViewController (UIStoryboardSegueBlocksPrivate)

@dynamic seguePerformBlocksDictionary;

static char seguePerformBlocksDictionaryKey;

-(NSMutableDictionary *)seguePerformBlocksDictionary
{
    return objc_getAssociatedObject(self, &seguePerformBlocksDictionaryKey);
}

-(void)setSeguePerformBlocksDictionary:(NSMutableDictionary *)seguePerformBlocksDictionary
{
    objc_setAssociatedObject(self, &seguePerformBlocksDictionaryKey, seguePerformBlocksDictionary, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end


@implementation UIViewController (UIStoryboardSegueBlocks)

-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender seguePerformBlock:(UIStoryboardSeguePerformBlock)seguePerformBlock
{
    if (self.seguePerformBlocksDictionary == nil)
    {
        self.seguePerformBlocksDictionary = [NSMutableDictionary dictionary];
    }
    
    //    [[seguePerformBlock copy] autorelease];
    
    [self.seguePerformBlocksDictionary setObject:[seguePerformBlock copy] forKey:identifier];
    
    [self performSegueWithIdentifier:identifier sender:sender];
}

@end

@implementation MHBlockSegue

-(void)perform
{
    UIViewController *source = self.sourceViewController;
    
    
    UIStoryboardSeguePerformBlock seguePerformBlock = [source.seguePerformBlocksDictionary objectForKey:self.identifier];
    
    if (seguePerformBlock)
    {
        seguePerformBlock(self);
    }
    
    [source.seguePerformBlocksDictionary removeObjectForKey:self.identifier];
}

@end