//
//  UIImage+UIColor.m
//  Pizza
//
//  Created by Milen Halachev on 3/20/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import "UIImage+UIColor.h"

@implementation UIImage (UIColor)

//+ (UIImage *) imageFromColor:(UIColor *)color frame:(CGRect)aFrame;
//{
////    CGRect rect = CGRectMake(0, 0, 1, 1);
//    CGRect rect = aFrame;
//    rect.origin.x = 0;
//    rect.origin.y = 0;
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [color CGColor]);
//    //  [[UIColor colorWithRed:222./255 green:227./255 blue: 229./255 alpha:1] CGColor]) ;
//    CGContextFillRect(context, rect);
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return img;
//}

+ (UIImage *)imageWithColor:(UIColor *)color 
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
