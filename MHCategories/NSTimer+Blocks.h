//
//  NSTimer+Blocks.h
//  MHCategories
//
//  Created by Milen Halachev on 6/3/13.
//  Copyright (c) 2013 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSTimer (Blocks)

+(id)scheduledTimerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)())inBlock repeats:(BOOL)inRepeats;
+(id)timerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)())inBlock repeats:(BOOL)inRepeats;

@end
