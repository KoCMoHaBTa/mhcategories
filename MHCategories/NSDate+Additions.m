//
//  NSDate+PRNumber.m
//  PhoneRace
//
//  Created by Milen Halachev on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (NSDate_Additions)

- (NSNumber*) toPRNumber
{
    if (self != nil) 
    {
        NSString *stringRepresentation = [NSString stringWithFormat:@"%.0f", [self timeIntervalSince1970]];
        
        NSNumber *result = [NSNumber numberWithLongLong:[stringRepresentation longLongValue]*1000];
        
        return result;
    }
    else return nil;
}

- (NSNumber*) year
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSNumber *year = [NSNumber numberWithInteger:[[calendar components:NSYearCalendarUnit fromDate:self] year]];
    
    return year;
}

- (NSArray*) yearsSince1900
{
    NSMutableArray *result = [NSMutableArray array];
    
    int min = 1900;
    int max = [[self year] intValue];
    int diff = max - min + 1;
            
    for (int i=0; i<diff; i++) 
    {
        @autoreleasepool {
        
            [result addObject:[NSString stringWithFormat:@"%i", min+i]];
    
        }
//        [pool release];
    }

    return result;
}

-(NSString *)stringFormatOfDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString*) stringFormatOfTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString*) stringFormatOfTimeWithSeconds
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterNoStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

-(NSString *)stringFormatOfDateAndTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSString *dateString = [dateFormatter stringFromDate:self];
    
    return dateString;
}

- (NSString*) stringFormatOfDateAndTimeWithTodayAndTomorrow
{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
//    
//    NSString *dateString = [dateFormatter stringFromDate:self];
//    [dateFormatter release];
//    
//    return dateString;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *now = [NSDate date];
    NSDateComponents *nowComponents = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:now];
    
    NSDateComponents *selfComponents = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
    
    if (selfComponents.year == nowComponents.year) 
    {
        if (selfComponents.month == nowComponents.month) 
        {
            if (selfComponents.day == nowComponents.day) 
            {
                //today
                NSString *result = [NSString stringWithFormat:@"Today, %@:", [self stringFormatOfTime]];
                return result;
            }
            
            if (selfComponents.day == nowComponents.day+1) 
            {
                //tomorrow
                NSString *result = [NSString stringWithFormat:@"Tomorrow, %@:", [self stringFormatOfTime]];
                return result;
            }
        }
    }
    
    return [self stringFormatOfDateAndTime];
}

//86400
- (NSDate *)dateByAddingDays:(NSNumber *)days
{
    int oneDay = 86400;
    int totalDays = [days intValue]*oneDay;
    return [self dateByAddingTimeInterval:totalDays];
}

-(NSDate *)dateByAddingMinutes:(NSNumber *)minutes
{
    int oneMinute = 60;
    int totalMinutes = [minutes intValue]*oneMinute;
    return [self dateByAddingTimeInterval:totalMinutes];
}

-(NSInteger)minutes
{
    NSDateComponents *components = nil;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    components = [calendar components:NSMinuteCalendarUnit fromDate:self];
    return components.minute;
}

-(NSInteger)days
{
    NSDateComponents *components = nil;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    components = [calendar components:NSDayCalendarUnit fromDate:self];
    return components.minute;
}

-(NSDate *)dateByRoundingToMinutesInterval:(NSInteger)minutesInterval_
{
    NSDate *date = self;
    NSInteger minutesInterval = minutesInterval_;
    NSInteger totalMinutes = 60;
    NSInteger intervalSteps = totalMinutes / minutesInterval;
    NSInteger intervalError = totalMinutes % minutesInterval;
    BOOL isIntervalCorrect = !intervalError;
    
    
    if (isIntervalCorrect)
    {
        NSInteger minutes = [date minutes];
        NSInteger nextInterval = 0;
        
        for (int i=0; i<intervalSteps; i++)
        {
            nextInterval = minutesInterval * i;
            
            if (nextInterval >= minutes)
            {
                break;
            }
        }
        
        NSInteger minutesToAdd = nextInterval - minutes;
        
        if (minutesToAdd < 0)
        {
            minutesToAdd +=  minutesInterval;
        }
        
        return [date dateByAddingMinutes:[NSNumber numberWithInteger:minutesToAdd]];

    }
    
    return date;
}

//-(NSDate *)dateByRoundingSecondsToZero
//{
//    
//}

@end
