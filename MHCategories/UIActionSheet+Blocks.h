//
//  UIActionSheet+Blocks.h
//  MHCategories
//
//  Created by Milen Halachev on 3/7/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <UIKit/UIKit.h>

//generic blocks
typedef void (^UIActionSheetBlock)(UIActionSheet* actionSheet);
typedef void (^UIActionSheetButtonIndexBlock)(UIActionSheet *actionSheet, NSInteger buttonIndex);

//delegate blocks
typedef UIActionSheetButtonIndexBlock UIActionSheetButtonClickedBlock;
typedef UIActionSheetBlock UIActionSheetCancelBlock;
typedef UIActionSheetBlock UIActionSheetWillPresentBlock;
typedef UIActionSheetBlock UIActionSheetDidPresentBlock;
typedef UIActionSheetButtonIndexBlock UIActionSheetWillDismissBlock;
typedef UIActionSheetButtonIndexBlock UIActionSheetDidDismissBlock;


@interface UIActionSheet (Blocks)

@property(nonatomic, copy) UIActionSheetButtonClickedBlock buttonClickedBlock;
@property(nonatomic, copy) UIActionSheetCancelBlock cancelBlock;
@property(nonatomic, copy) UIActionSheetWillPresentBlock willPresentBlock;
@property(nonatomic, copy) UIActionSheetDidPresentBlock didPresentBlock;
@property(nonatomic, copy) UIActionSheetWillDismissBlock willDismissBlock;
@property(nonatomic, copy) UIActionSheetDidDismissBlock didDismissBlock;

+(UIActionSheet*)actionSheetWithTitle:(NSString*)title 
                         dismissBlock:(UIActionSheetDidDismissBlock)dismissBlock 
                    cancelButtonTitle:(NSString *)cancelButtonTitle 
               destructiveButtonTitle:(NSString *)destructiveButtonTitle 
                    otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

@end
