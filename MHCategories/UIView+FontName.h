//
//  NSObject+FontName.h
//  MHCategories
//
//  Created by Milen Halachev on 5/14/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (FontName)

@property(nonatomic, strong) NSString *fontName UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong) NSString *fontKey UI_APPEARANCE_SELECTOR;
@property(nonatomic, strong) UIFont *font UI_APPEARANCE_SELECTOR;

+(void)registerFontName:(NSString*)fontName forKey:(NSString*)fontKey symbolicTraits:(UIFontDescriptorSymbolicTraits)symbolicTraits;
+(void)excludeFontAppearanceFromContainer:(Class)container;

@end
