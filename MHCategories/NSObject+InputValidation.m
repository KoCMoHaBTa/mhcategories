//
//  UITextField+InputValidation.m
//  MHCategories
//
//  Created by Milen Halachev on 5/30/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "NSObject+InputValidation.h"
#import <objc/runtime.h>

@interface NSObject (_InputValidation)

@property(nonatomic, strong) NSMutableArray *inputValidationBlocks;

@end

@implementation NSObject (_InputValidation)

@dynamic inputValidationBlocks;
static char inputValidationBlocksKey;

-(NSMutableArray *)inputValidationBlocks
{
    return objc_getAssociatedObject(self, &inputValidationBlocksKey);
}

-(void)setInputValidationBlocks:(NSMutableArray *)inputValidationBlocks
{
    objc_setAssociatedObject(self, &inputValidationBlocksKey, inputValidationBlocks, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@implementation NSObject (InputValidation)

-(void)addInputValidationBlock:(NSObjectInputValidationBlock)block
{
    if (!self.inputValidationBlocks) 
    {
        self.inputValidationBlocks = [NSMutableArray array];
    }
    
    if ([self isKindOfClass:[UIControl class]]) 
    {
        UIControl *control = (UIControl*)self;
        [control addTarget:self action:@selector(validateInput) forControlEvents:UIControlEventEditingDidEnd];
    }
    
    [self.inputValidationBlocks addObject:[block copy]];
}

-(BOOL)validateInput
{
    BOOL result = YES;
    
    for (NSObjectInputValidationBlock block in self.inputValidationBlocks) 
    {
        result = result && block(self);
    }
    
    return result;
}

@end
