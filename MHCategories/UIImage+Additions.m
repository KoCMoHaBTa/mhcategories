//
//  UIImage+Additions.m
//  BGMenu
//
//  Created by Milen Halachev on 10/8/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import "UIImage+Additions.h"

@implementation UIImage (Additions)

+(UIImage *)imageWithImages:(UIImage *)images, ...
{
    NSMutableArray *_images = [NSMutableArray array];
    CGSize maxSize = CGSizeZero;
    
    if (images)
    {
        maxSize = images.size;
        [_images addObject:images];
        
        va_list arguments;
        va_start(arguments, images);
        
        UIImage *image = nil;
        while ((image = va_arg(arguments, id)))
        {
            if (image.size.height > maxSize.height)
            {
                maxSize.height = image.size.height;
            }
            
            if (image.size.width > maxSize.width)
            {
                maxSize.width = image.size.width;
            }
            
            [_images addObject:image];
        }
        
        va_end(arguments);
    }
    
    UIGraphicsBeginImageContext(maxSize);
    
    for (UIImage *image in _images)
    {
        [image drawInRect:CGRectMake(0, 0, maxSize.width, maxSize.height)];
    }
    
    UIImage *multiImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return multiImage;
    
//    UIImage *bottomImage = [UIImage imageNamed:@"bottom.png"];
//    UIImage *image = [UIImage imageNamed:@"top.png"];
//    
//    CGSize newSize = CGSizeMake(width, height);
//    UIGraphicsBeginImageContext( newSize );
//    
//    // Use existing opacity as is
//    [bottomImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//    // Apply supplied opacity
//    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height) blendMode:kCGBlendModeNormal alpha:0.8];
//    
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    UIGraphicsEndImageContext();

}

-(void)addImage:(UIImage *)image
{
    UIGraphicsBeginImageContext(self.size);
    
    [image drawInRect:CGRectMake(0, 0, self.size.width, self.size.height)];
    
    UIGraphicsEndImageContext();
}

@end
