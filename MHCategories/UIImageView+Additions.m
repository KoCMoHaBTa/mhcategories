//
//  UIImageView+Additions.m
//  BGMenu
//
//  Created by Milen Halachev on 10/9/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import "UIImageView+Additions.h"

@implementation UIImageView (Additions)

-(void)setShadowBorderWithColor:(UIColor *)shadowColor_
                    borderColor:(UIColor *)borderColor_
                    borderWidth:(CGFloat)borderWidth_
                   cornerRadius:(CGFloat)cornerRadius_
                   shadowRadius:(CGFloat)shadowRadius_
{
    UIImageView *imageView = self;
    
    CGFloat cornerRadius = cornerRadius_;
    CGFloat shadowRadius = shadowRadius_;
    CGFloat borderPX = borderWidth_;
    CGColorRef shadowColor = shadowColor_.CGColor;
    CGColorRef borderColor = borderColor_.CGColor;
    
    CALayer *imageLayer = imageView.layer;
    
    //make the image layer Xpx smaller
    imageLayer.frame = CGRectMake(imageLayer.frame.origin.x + borderPX,
                                  imageLayer.frame.origin.y + borderPX,
                                  imageLayer.frame.size.width - borderPX*2,
                                  imageLayer.frame.size.height - borderPX*2);
    
    
    CALayer *shadowLayer = [CALayer layer];
    
    //make the shadow layer Xpx larger than image layer
    shadowLayer.frame = CGRectMake(-borderPX,
                                   -borderPX,
                                   imageLayer.bounds.size.width + borderPX*2,
                                   imageLayer.bounds.size.height + borderPX*2);
    
    shadowLayer.shadowColor = shadowColor;
    shadowLayer.shadowOffset = CGSizeMake(0, 0);
    shadowLayer.shadowOpacity = 0.65;
    shadowLayer.shadowRadius = shadowRadius;
    
    shadowLayer.borderWidth = borderPX; //2
    shadowLayer.borderColor = borderColor;
    shadowLayer.cornerRadius = cornerRadius;
    shadowLayer.masksToBounds = NO;
    
    
    
    imageLayer.cornerRadius = cornerRadius;
    imageLayer.masksToBounds = NO;
    
    CALayer *maskLayer = [CALayer layer];
    
    //make the mask layer Xpx larger than the shadow layer
    maskLayer.frame = CGRectMake(-borderPX,
                                 -borderPX,
                                 shadowLayer.bounds.size.width + borderPX*2,
                                 shadowLayer.bounds.size.height + borderPX*2);
    
    maskLayer.borderWidth = borderPX*2;  //4
    maskLayer.borderColor = [UIColor whiteColor].CGColor;
    maskLayer.backgroundColor = [UIColor clearColor].CGColor;
    
    maskLayer.cornerRadius = cornerRadius;
    
    shadowLayer.mask = maskLayer;
    
    
    
    [imageLayer addSublayer:shadowLayer];
}

-(void)setDefaultBorderShadow
{
    UIColor *shadowColor = [UIColor blackColor];
    UIColor *borderColor = [UIColor whiteColor];
    CGFloat borderWidth = 2;
    CGFloat cornerRadius = 2;
    CGFloat shadowRadius = 2;
    
    [self setShadowBorderWithColor:shadowColor borderColor:borderColor borderWidth:borderWidth cornerRadius:cornerRadius shadowRadius:shadowRadius];
}

@end
