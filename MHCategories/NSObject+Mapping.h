//
//  NSObject+Mapping.h
//  DelX
//
//  Created by Milen Halachev on 2/24/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id(^MHObjectMappingAllocationBlock)(void);

//create your own protocol that conforms to this and override the genericType with the desired type. Then declare your Generic protocol to the NSArray property
@protocol MHGenerig <NSObject>
@optional
@property(nonatomic, assign) id genericType;
@end

//declare to a property in order to mark it as mappable, otherwise it will be skippped
@protocol MHMappable <NSObject>
@end

//declare to a property in order to mark it as serializable, otherwise it will be skippped
@protocol MHSerializable <NSObject>
@end

@interface NSObject (Mapping)

//constructors
+(instancetype)objectWithDictionary:(NSDictionary*)dictionary;
+(NSArray*)objectsWithArray:(NSArray*)array;
+(id)createWithObject:(id)object; //NSDictionary ot NSArray to instancetype or NSArray

//Mapping
-(void)mapValue:(id)value forKey:(NSString *)key;
-(void)mapValuesForKeysWithDictionary:(NSDictionary *)keyedValues;

//Serializing
-(NSDictionary*)serializeToDictionary; //returns a mapping dictionary of the object

//mapping info - subclasses of NSObject can override these methods
+(MHObjectMappingAllocationBlock)mappingAllocationBlock; //Returns a block that allocates and instance of the class. Defaults to [[[self class] alloc] init];
//+(Class)classForTargetWithKey:(NSString*)key; //Returns the class if the target itself (if object) or elements (if array). If Nil - set the value directly. If SkipClass - skip this key-value pair and does not set anything. Default implementation tries to lookup the class automatically

+(NSArray*)genericProtocols; //return an array of all generic protocols used. This method is required. If there are no generic protocols used - return nil.

-(Class)classForTargetObjectWithKey:(NSString*)key;
-(Class)classForElementsInTargetCollectionWithKey:(NSString*)key;

+(BOOL)shouldReverseMappableCheck; //Defaults to NO. If you returns YES, properties declared with MHMappable will be skipped and these not marked - will be mapped
+(void)setShouldReverseMappableCheck:(BOOL)shouldReverse;
+(BOOL)shouldReverseSerializableCheck; //Defaults to NO. If you returns YES, properties declared with MHSerializable will be skipped and these not marked - will be serialized
+(void)setShouldReverseSerializableCheck:(BOOL)shouldReverse;

-(NSArray*)keysToMap;
-(NSArray*)keysToSkipMap;
-(NSArray*)keysToSerialize;
-(NSArray*)keysToSkipSerialize;

//you can @synthesize properties in order to perform key mapping

@end
