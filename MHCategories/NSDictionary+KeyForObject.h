//
//  NSDictionary+KeyForValue.h
//  FastMenuCore
//
//  Created by Milen Halachev on 7/13/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (KeyForObject)

-(id)firstKeyForObject:(id)obj;


@end

@interface NSMutableDictionary (KeyForObject)

-(void)removeObject:(id)obj;

@end