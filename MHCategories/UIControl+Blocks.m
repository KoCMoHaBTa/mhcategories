//
//  UIControl+Blocks.m
//  BGMenu
//
//  Created by Milen Halachev on 10/23/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import "UIControl+Blocks.h"
#import <objc/runtime.h>

@interface UIControl (BlocksPrivate)

@property(nonatomic, retain) NSMutableDictionary *actionBlocksTable;

-(NSMutableArray*)blocksArrayForEvent:(NSNumber*)event;

//forward methods
-(void)forwardUIControlEventTouchDownToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchDownRepeatToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchDragInsideToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchDragOutsideToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchDragEnterToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchDragExitToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchUpInsideToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchUpOutsideToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventTouchCancelToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventValueChangedToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventEditingDidBeginToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventEditingChangedToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventEditingDidEndToBlock:(id)sender event:(id)event;
-(void)forwardUIControlEventEditingDidEndOnExitToBlock:(id)sender event:(id)event;

//dynamic mapping
-(NSArray*)eventActions:(UIControlEvents)events;

//static mapping
-(NSArray*)eventNumbers:(UIControlEvents)events;
-(NSArray*)eventStrings:(UIControlEvents)events;

@end



@implementation UIControl (Blocks)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmismatched-parameter-types"

-(void)addActionBlock:(id)actionBlock forEvents:(UIControlEvents)events
{    
    NSArray *eventNumbers = [self eventNumbers:events];
    for (NSNumber *eventNumber in eventNumbers) 
    {
        NSArray *eventActions = [self eventActions:[eventNumber integerValue]];
        NSString *action =  [eventActions firstObject];
        SEL selector = NSSelectorFromString(action);
        
        [self addTarget:self action:selector forControlEvents:[eventNumber integerValue]];
        
        [[self blocksArrayForEvent:eventNumber] addObject:[actionBlock copy]];
    }
}

#pragma clang diagnostic pop

//-(NSArray *)actionBlocksForEvents:(UIControlEvents)events
//{
//    NSMutableArray *result = [NSMutableArray array];
//    
//    NSArray *eventNumbers = [self eventNumbers:events];
//    for (NSNumber *eventNumber in eventNumbers) 
//    {
//        [result addObjectsFromArray:[self blocksArrayForEvent:eventNumber]];
//    }
//    
//    return [NSArray arrayWithArray:result];
//}

-(void)removeAllActionBlocksForEvents:(UIControlEvents)events
{
    NSArray *eventNumbers = [self eventNumbers:events];
    for (NSNumber *eventNumber in eventNumbers) 
    {
        [[self blocksArrayForEvent:eventNumber] removeAllObjects];
    }
}

@end




@implementation UIControl (BlocksPrivate)

#pragma mark - Action Blocks Table

@dynamic actionBlocksTable;

static char actionBlocksTableKey;

-(NSMutableDictionary *)actionBlocksTable
{
    NSMutableDictionary *actionBlocksTable = objc_getAssociatedObject(self, &actionBlocksTableKey);
    
    if (!actionBlocksTable) 
    {
        actionBlocksTable = [NSMutableDictionary dictionary];
        [self setActionBlocksTable:actionBlocksTable];
    }
    
    return actionBlocksTable;
}

-(void)setActionBlocksTable:(NSMutableDictionary *)actionBlocksTable
{
    objc_setAssociatedObject(self, &actionBlocksTableKey, actionBlocksTable, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Blocks Array 

-(NSMutableArray *)blocksArrayForEvent:(NSNumber *)event
{
    NSMutableArray *blocksArray = self.actionBlocksTable[event];
    
    if (!blocksArray) 
    {
        blocksArray = [NSMutableArray array];
        [self.actionBlocksTable setObject:blocksArray forKey:event];
    }
    
    return blocksArray;
}

#pragma mark - Forward Methods

-(void)forwardUIControlEventTouchDownToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchDown)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchDownRepeatToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchDownRepeat)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchDragInsideToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchDragInside)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchDragOutsideToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchDragOutside)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchDragEnterToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchDragEnter)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchDragExitToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchDragExit)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchUpInsideToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchUpInside)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchUpOutsideToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchUpOutside)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventTouchCancelToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventTouchCancel)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventValueChangedToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventValueChanged)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventEditingDidBeginToBlock:(id)sender event:(id)event
{
    UIControlEvents events = UIControlEventEditingDidBegin;
    NSNumber *eventNumber = @(events);
    NSArray *blocks = [self blocksArrayForEvent:eventNumber];
    
    for (UIControlActionBlock actionBlock in blocks) 
    {
        actionBlock(sender, event);
        NSLog(@"");
    }
}

-(void)forwardUIControlEventEditingChangedToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventEditingChanged)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventEditingDidEndToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventEditingDidEnd)]) 
    {
        actionBlock(sender, event);
    }
}

-(void)forwardUIControlEventEditingDidEndOnExitToBlock:(id)sender event:(id)event
{
    for (UIControlActionBlock actionBlock in [self blocksArrayForEvent:@(UIControlEventEditingDidEndOnExit)]) 
    {
        actionBlock(sender, event);
    }
}

#pragma mark - Dynamic Mapping

-(NSArray *)eventActions:(UIControlEvents)events
{
    NSMutableArray *actions = [NSMutableArray array];
    
    for (NSString *eventString in [self eventStrings:events]) 
    {
        [actions addObject:[NSString stringWithFormat:@"forward%@ToBlock:event:", eventString]];
    }
    
    return [NSArray arrayWithArray:actions];
}

#pragma mark - Static Mapping

-(NSArray *)eventNumbers:(UIControlEvents)events
{
    NSMutableArray *result = [NSMutableArray array];
    
    if (events & UIControlEventTouchDown) 
    {
        [result addObject:@(UIControlEventTouchDown)];
    }
    
    if (events & UIControlEventTouchDownRepeat) 
    {
        [result addObject:@(UIControlEventTouchDownRepeat)];
    }
    
    if (events & UIControlEventTouchDragInside) 
    {
        [result addObject:@(UIControlEventTouchDragInside)];
    }
    
    if (events & UIControlEventTouchDragOutside) 
    {
        [result addObject:@(UIControlEventTouchDragOutside)];
    }
    
    if (events & UIControlEventTouchDragEnter) 
    {
        [result addObject:@(UIControlEventTouchDragEnter)];
    }
    
    if (events & UIControlEventTouchDragExit) 
    {
        [result addObject:@(UIControlEventTouchDragExit)];
    }
    
    if (events & UIControlEventTouchUpInside) 
    {
        [result addObject:@(UIControlEventTouchUpInside)];
    }
    
    if (events & UIControlEventTouchUpOutside) 
    {
        [result addObject:@(UIControlEventTouchUpOutside)];
    }
    
    if (events & UIControlEventTouchCancel) 
    {
        [result addObject:@(UIControlEventTouchCancel)];
    }
    
    if (events & UIControlEventValueChanged) 
    {
        [result addObject:@(UIControlEventValueChanged)];
    }
    
    if (events & UIControlEventEditingDidBegin) 
    {
        [result addObject:@(UIControlEventEditingDidBegin)];
    }
    
    if (events & UIControlEventEditingChanged) 
    {
        [result addObject:@(UIControlEventEditingChanged)];
    }
    
    if (events & UIControlEventEditingDidEnd) 
    {
        [result addObject:@(UIControlEventEditingDidEnd)];
    }
    
    if (events & UIControlEventEditingDidEndOnExit) 
    {
        [result addObject:@(UIControlEventEditingDidEndOnExit)];
    }
    
    return [NSArray arrayWithArray:result];
}

-(NSArray *)eventStrings:(UIControlEvents)events
{
    NSMutableArray *result = [NSMutableArray array];
    
    if (events & UIControlEventTouchDown) 
    {
        [result addObject:@"UIControlEventTouchDown"];
    }
    
    if (events & UIControlEventTouchDownRepeat) 
    {
        [result addObject:@"UIControlEventTouchDownRepeat"];
    }
    
    if (events & UIControlEventTouchDragInside) 
    {
        [result addObject:@"UIControlEventTouchDragInside"];
    }
    
    if (events & UIControlEventTouchDragOutside) 
    {
        [result addObject:@"UIControlEventTouchDragOutside"];
    }
    
    if (events & UIControlEventTouchDragEnter) 
    {
        [result addObject:@"UIControlEventTouchDragEnter"];
    }
    
    if (events & UIControlEventTouchDragExit) 
    {
        [result addObject:@"UIControlEventTouchDragExit"];
    }
    
    if (events & UIControlEventTouchUpInside) 
    {
        [result addObject:@"UIControlEventTouchUpInside"];
    }
    
    if (events & UIControlEventTouchUpOutside) 
    {
        [result addObject:@"UIControlEventTouchUpOutside"];
    }
    
    if (events & UIControlEventTouchCancel) 
    {
        [result addObject:@"UIControlEventTouchCancel"];
    }
    
    if (events & UIControlEventValueChanged) 
    {
        [result addObject:@"UIControlEventValueChanged"];
    }
    
    if (events & UIControlEventEditingDidBegin) 
    {
        [result addObject:@"UIControlEventEditingDidBegin"];
    }
    
    if (events & UIControlEventEditingChanged) 
    {
        [result addObject:@"UIControlEventEditingChanged"];
    }
    
    if (events & UIControlEventEditingDidEnd) 
    {
        [result addObject:@"UIControlEventEditingDidEnd"];
    }
    
    if (events & UIControlEventEditingDidEndOnExit) 
    {
        [result addObject:@"UIControlEventEditingDidEndOnExit"];
    }
    
    return [NSArray arrayWithArray:result];
}

@end

