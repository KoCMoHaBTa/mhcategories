//
//  UIViewController+IBRuntimeAttributes.h
//  FastMenu
//
//  Created by Milen Halachev on 11/5/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (IBRuntimeAttributes)
    
@property(nonatomic, assign) BOOL hideNavigationBar;
@property(nonatomic, assign) BOOL returnSELFAsNavigationController;
@property(nonatomic, assign) BOOL showToolBar;
@property(nonatomic, assign) BOOL hideTabBar;

-(IBAction)dismissModalViewControllerUnwindSegue:(UIStoryboardSegue*)sender;
-(IBAction)dismissModalViewControllerAction:(id)sender;

-(IBAction)testAction:(id)sender;
-(IBAction)pushSegueAction:(id)sender;

-(void)recursiveLoadView;

@end
