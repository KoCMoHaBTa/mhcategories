//
//  NSDictionary+KeyForValue.m
//  FastMenuCore
//
//  Created by Milen Halachev on 7/13/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import "NSDictionary+KeyForObject.h"

@implementation NSDictionary (KeyForObject)

-(id)firstKeyForObject:(id)obj
{
    NSArray *allKeysForValue = [self allKeysForObject:obj];
    
    if (allKeysForValue == nil || allKeysForValue.count == 0)
    {
        return nil;
    }
    
    if (allKeysForValue.count > 1)
    {
        for (id key in allKeysForValue)
        {
            id existingObject = [self objectForKey:key];
            if (existingObject == obj)
            {
                return key;
            }
        }
    }
    
    return [allKeysForValue objectAtIndex:0];
}



@end


@implementation NSMutableDictionary (KeyForObject)

-(void)removeObject:(id)obj
{
    id key = [self firstKeyForObject:obj];
    [self removeObjectForKey:key];
}

@end