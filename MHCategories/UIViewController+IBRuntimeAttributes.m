//
//  UIViewController+IBRuntimeAttributes.m
//  FastMenu
//
//  Created by Milen Halachev on 11/5/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import "UIViewController+IBRuntimeAttributes.h"
#import <objc/runtime.h>
#import "NSObject+Hooking.h"

@implementation UIViewController (IBRuntimeAttributes)
    
@dynamic hideNavigationBar;
static char hideNavigationBarKey;
    
-(BOOL)hideNavigationBar
{
    NSNumber *val = objc_getAssociatedObject(self, &hideNavigationBarKey);
    return [val boolValue];
}

-(void)setHideNavigationBar:(BOOL)hideNavigationBar
{
    objc_setAssociatedObject(self, &hideNavigationBarKey, @(hideNavigationBar), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
    

@dynamic returnSELFAsNavigationController;
static char returnSELFAsNavigationControllerKey;

-(BOOL)returnSELFAsNavigationController
{
    NSNumber *val = objc_getAssociatedObject(self, &returnSELFAsNavigationControllerKey);
    return [val boolValue];
}

-(void)setReturnSELFAsNavigationController:(BOOL)returnSELFAsNavigationController
{
    objc_setAssociatedObject(self, &returnSELFAsNavigationControllerKey, @(returnSELFAsNavigationController), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@dynamic showToolBar;
static char showToolBarKey;

-(BOOL)showToolBar
{
    NSNumber *val = objc_getAssociatedObject(self, &showToolBarKey);
    return [val boolValue];
}

-(void)setShowToolBar:(BOOL)showToolBar
{
    objc_setAssociatedObject(self, &showToolBarKey, @(showToolBar), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@dynamic hideTabBar;
static char hideTabBarKey;

-(BOOL)hideTabBar
{
    NSNumber *val = objc_getAssociatedObject(self, &hideTabBarKey);
    return [val boolValue];
}

-(void)setHideTabBar:(BOOL)hideTabBar
{
    objc_setAssociatedObject(self, &hideTabBarKey, @(hideTabBar), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
    
+(void)load
{
    [self hookSourceClass:[self class] sourceMethod:@selector(viewWillAppear:) destinationClass:[self class] destinationMethod:@selector(ibra_viewWillAppear:)];
    [self hookSourceClass:[self class] sourceMethod:@selector(viewWillDisappear:) destinationClass:[self class] destinationMethod:@selector(ibra_viewWillDisappear:)];
    [self hookSourceClass:[self class] sourceMethod:@selector(navigationController) destinationClass:[self class] destinationMethod:@selector(ibra_navigationController)];
    
//    [self hookSourceClass:[self class] sourceMethod:<#(SEL)#> destinationClass:[self class] destinationMethod:<#(SEL)#>]

}

-(void)ibra_viewWillAppear:(BOOL)animated
{
    [self ibra_viewWillAppear:animated];
    
    if (self.hideNavigationBar) 
    {
        [self.navigationController setNavigationBarHidden:self.hideNavigationBar animated:animated];
    }
    
    if (self.showToolBar) 
    {
        [self.navigationController setToolbarHidden:!self.showToolBar animated:animated];
    }
    
    if (self.hideTabBar) 
    {
        [self.tabBarController.tabBar setHidden:self.hideTabBar];
    }
}

-(void)ibra_viewWillDisappear:(BOOL)animated
{
    [self ibra_viewWillDisappear:animated];
    
    if (self.hideNavigationBar) 
    {
        [self.navigationController setNavigationBarHidden:!self.hideNavigationBar animated:animated];
    }
    
    if (self.showToolBar) 
    {
        [self.navigationController setToolbarHidden:self.showToolBar animated:animated];
    }
    
    if (self.hideTabBar) 
    {
        [self.tabBarController.tabBar setHidden:!self.hideTabBar];
    }
}


-(void)testAction:(id)sender
{
//    [[self.navigationController containerViewController] setFooterViewHidden:!self.navigationController.containerViewController.isFooterViewHidden animated:YES];
    
    NSLog(@"");
}

-(void)pushSegueAction:(id)sender
{
    [self performSegueWithIdentifier:@"Push" sender:sender];
}


-(UINavigationController *)ibra_navigationController
{
    if (self.returnSELFAsNavigationController) 
    {
        return (id)self;
    }
    
    return [self ibra_navigationController];
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}

-(IBAction)dismissModalViewControllerUnwindSegue:(UIStoryboardSegue*)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)dismissModalViewControllerAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

//void(^TransferControlActions)(UIControl *control, id sourceTarget, id destinationTarget, UIControlEvents event) = ^(UIControl *control, id sourceTarget, id destinationTarget, UIControlEvents event) 
//{
//    NSArray *actions = [control actionsForTarget:sourceTarget forControlEvent:event];
//    for (NSString *action in actions) 
//    {
//        [control removeTarget:sourceTarget action:NSSelectorFromString(action) forControlEvents:event];
//        [control addTarget:destinationTarget action:NSSelectorFromString(action) forControlEvents:event];
//    }
//};


-(void)recursiveLoadView
{
    [self view];
    
//    if ([self isKindOfClass:[UINavigationController class]]) 
//    {
//        [[(UINavigationController*)self viewControllers] makeObjectsPerformSelector:@selector(recursiveLoadView)];
//    }
    
    if ([self respondsToSelector:@selector(viewControllers)]) 
    {
        [[self performSelector:@selector(viewControllers) withObject:nil] makeObjectsPerformSelector:@selector(recursiveLoadView)];
    }
}

@end
