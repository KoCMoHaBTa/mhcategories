//
//  UIView+Additions.h
//  House Doctor
//
//  Created by Milen Halachev on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIView_Additions)
{
    
}

-(void) setRoundedCornersWithRadius:(CGFloat)radius borderColor:(CGColorRef)cgColor borderWidth:(CGFloat)borderWidth;
-(void) setRoundedCorners:(BOOL)rounded;


@end
