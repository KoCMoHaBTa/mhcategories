//
//  UIStoryboardSegue+Blocks.h
//  MHCategories
//
//  Created by Milen Halachev on 8/12/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboardSegue (Blocks)

@end


//segue perform block
typedef void(^UIStoryboardSeguePerformBlock)(UIStoryboardSegue *segue);

//UIViewController category for block segue usage
@interface UIViewController (UIStoryboardSegueBlocks)

-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender seguePerformBlock:(UIStoryboardSeguePerformBlock)seguePerformBlock;

@end

//block segue
@interface MHBlockSegue : UIStoryboardSegue

@end