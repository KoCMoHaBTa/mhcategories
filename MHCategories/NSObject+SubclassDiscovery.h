//
//  NSObject+SubclassDiscovery.h
//  FastMenuCore
//
//  Created by Milen Halachev on 11/27/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (SubclassDiscovery)

+ (NSArray*)allSubclasses;
+ (NSArray*)allDirectSubclasses;
+ (NSArray*)allIvarKeys;

@end
