//
//  NSObject+KeyValueDictionary.m
//  FastMenuCore
//
//  Created by Milen Halachev on 7/11/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import "NSObject+KeyValueDictionary.h"
#import <objc/runtime.h>

@implementation NSObject (KeyValueDictionary)


-(NSArray*)propertiesArrayForClass:(Class)class
{
    NSMutableArray *propertiesArray = [NSMutableArray array];
    
    if (class.superclass != [NSObject class])
    {
        NSArray *superclassPropertiesArray = [self propertiesArrayForClass:class.superclass];
        [propertiesArray addObjectsFromArray:superclassPropertiesArray];
    }
    
    unsigned int propertiesCount;
    objc_property_t *properties = class_copyPropertyList(class, &propertiesCount);
    for (int i=0; i<propertiesCount; i++)
    {
        objc_property_t property = properties[i];
        
        NSValue *value = [NSValue value:&property withObjCType:@encode(objc_property_t)];
        
        [propertiesArray addObject:value];
    }
    free(properties);
    
    return propertiesArray;
}

-(NSMutableDictionary *)keyValueDictionaryIncludingCollections:(BOOL)includeCollections
{
    NSMutableDictionary *keyValueDictionary = [NSMutableDictionary dictionary];
    
    NSArray *properties = [self propertiesArrayForClass:self.class];
    
    for (id obj in properties)
    {
        NSValue *value = obj;
        objc_property_t property = nil;
        property = [value pointerValue];
        
        NSString *propertyKey = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        id propertyValue = [self valueForKey:propertyKey];
        
        if (propertyValue)
        {
            BOOL addToDictionary = YES;
            
            if (includeCollections == NO)
            {
                if ([propertyValue isKindOfClass:[NSArray class]] || [propertyValue isKindOfClass:[NSSet class]])
                {
                    addToDictionary = NO;
                }
            }
            
            if (addToDictionary)
            {
                // -- //
                //this is for printing
//                if ([propertyValue isKindOfClass:[NSArray class]] || [propertyValue isKindOfClass:[NSSet class]])
//                {
//                    propertyValue = [propertyValue keyValueDictionaryIncludingCollections:includeCollections];
//                }
                // -- //
                
                [keyValueDictionary setObject:propertyValue forKey:propertyKey];
                
            }
            
        }
        else
        {
            [keyValueDictionary setObject:[NSNull null] forKey:propertyKey];
        }
    }
    
    //in iOS 8 SDK, there are some methods converted to properties of NSObject - remove them
    [keyValueDictionary removeObjectForKey:@"hash"];
    [keyValueDictionary removeObjectForKey:@"superclass"];
    [keyValueDictionary removeObjectForKey:@"debugDescription"];
    [keyValueDictionary removeObjectForKey:@"description"];
    
    return keyValueDictionary;
    
}

-(NSMutableDictionary *)descpritionWithKeyValueDictionaryIncludingCollections:(BOOL)includeCollections
{
    NSMutableDictionary *keyValueDictionary = [NSMutableDictionary dictionary];
    
    NSArray *properties = [self propertiesArrayForClass:self.class];
    
    for (id obj in properties)
    {
        NSValue *value = obj;
        objc_property_t property = nil;
        property = [value pointerValue];
        
        NSString *propertyKey = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        id propertyValue = [self valueForKey:propertyKey];
        
        if (propertyValue)
        {
            BOOL addToDictionary = YES;
            
            if (includeCollections == NO)
            {
                if ([propertyValue isKindOfClass:[NSArray class]] || [propertyValue isKindOfClass:[NSSet class]])
                {
                    addToDictionary = NO;
                }
            }
            
            if (addToDictionary)
            {
                // -- //
                //this is for printing
                if ([propertyValue isKindOfClass:[NSArray class]] || [propertyValue isKindOfClass:[NSSet class]])
                {
                    propertyValue = [propertyValue descpritionWithKeyValueDictionaryIncludingCollections:includeCollections];
                }
                // -- //
                
                [keyValueDictionary setObject:propertyValue forKey:propertyKey];
                
            }
            
        }
        else
        {
            [keyValueDictionary setObject:[NSNull null] forKey:propertyKey];
        }
    }
    
    return keyValueDictionary;
}



@end

@implementation NSArray (KeyValueDictionary)

-(NSArray *)descpritionWithKeyValueDictionaryIncludingCollections:(BOOL)includeCollections
{
    NSMutableArray *arr = [NSMutableArray array];
    
    for (id obj in self)
    {
        NSDictionary *dict = [obj keyValueDictionaryIncludingCollections:includeCollections];
        [arr addObject:dict];
    }
    
    return arr;
}

@end

@implementation NSSet (KeyValueDictionary)

-(NSSet *)descpritionWithKeyValueDictionaryIncludingCollections:(BOOL)includeCollections
{
    NSMutableSet *set = [NSMutableSet set];
    
    for (id obj in self)
    {
        NSDictionary *dict = [obj keyValueDictionaryIncludingCollections:includeCollections];
        [set addObject:dict];
    }
    
    return set;
}

@end
