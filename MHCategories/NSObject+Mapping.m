//
//  NSObject+Mapping.m
//  DelX
//
//  Created by Milen Halachev on 2/24/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "NSObject+Mapping.h"
#import <objc/runtime.h>

NSString *const MHPropertyInfoKeyNameKey = @"key";
NSString *const MHPropertyInfoIvarNameKey = @"ivarName";
NSString *const MHPropertyInfoPropertyNameKey = @"propertyName";
NSString *const MHPropertyInfoClassNameKey = @"className";
NSString *const MHPropertyInfoGenericInfoKey = @"genericInformation";
NSString *const MHPropertyInfoDeclaredProtocolsNamesKey = @"declaredProtocolsNames";
NSString *const MHPropertyInfoShouldMapKey = @"shouldMap";
NSString *const MHPropertyInfoShouldSerializeKey = @"shouldSerialize";

NSString *const MHGenericTypePropertyName = @"genericType";

NSString *const MHPropertyTypeAttributeName = @"T";
NSString *const MHPropertyIvarAttributeName = @"V";

@implementation NSObject (Mapping)

#pragma mark - Constructors

+(instancetype)objectWithDictionary:(NSDictionary *)dictionary
{
    id instance = [self mappingAllocationBlock]();
    [instance mapValuesForKeysWithDictionary:dictionary];
    return instance;
}

+(NSArray *)objectsWithArray:(NSArray *)array
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:array.count];
    
    for (id obj in array) 
    {
        id instance = [self createWithObject:obj];
        [result addObject:instance];
    }
    
    return result;
}

+(id)createWithObject:(id)object
{    
    if ([object isKindOfClass:[NSArray class]]) 
    {
        return [self objectsWithArray:object];
    }
    
    if ([object isKindOfClass:[NSDictionary class]]) 
    {
        return [self objectWithDictionary:object];
    }
    
    [NSException raise:NSInvalidArgumentException format:@"Unsupported object type: %@", object];
    return nil;
}

#pragma mark - Mapping

-(void)mapValue:(id)value forKey:(NSString *)key
{    
    NSDictionary *info = [[self class] informationForTargetWithKey:key];
    
    BOOL shouldMap =  [info[MHPropertyInfoShouldMapKey] boolValue];
    
    BOOL wantsToMap = [[self keysToMap] containsObject:info[MHPropertyInfoKeyNameKey]] || [[self keysToMap] containsObject:info[MHPropertyInfoPropertyNameKey]] || [[self keysToMap] containsObject:info[MHPropertyInfoIvarNameKey]];
    BOOL wantsToSkipMap = [[self keysToSkipMap] containsObject:info[MHPropertyInfoKeyNameKey]] || [[self keysToSkipMap] containsObject:info[MHPropertyInfoPropertyNameKey]] || [[self keysToSkipMap] containsObject:info[MHPropertyInfoIvarNameKey]];
    
    BOOL shouldProceed = (shouldMap && !wantsToSkipMap) || (!shouldMap && wantsToMap);
    
    if (!shouldProceed) 
    {
        return;
    }
    
    if ([value isKindOfClass:[NSDictionary class]]) 
    {
        Class targetClass = [self classForTargetObjectWithKey:key];
        if (targetClass) 
        {            
            id target = [targetClass objectWithDictionary:value];
            value = target;

            goto map;
        }
    }
    
    if ([value isKindOfClass:[NSArray class]]) 
    {
        Class targetClass = [self classForElementsInTargetCollectionWithKey:key];
        if (targetClass) 
        {            
            id target = [targetClass objectsWithArray:value];
            value = target;
            
            goto map;
        }
    }
    
    if ([value isEqual:[NSNull null]]) 
    {
        value = nil;
    }
    
    map:
    [self setValue:value forKey:key];
}

-(void)mapValuesForKeysWithDictionary:(NSDictionary *)keyedValues
{
    [keyedValues enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) 
    {
        [self mapValue:obj forKey:key]; 
    }];
}

-(NSDictionary *)serializeToDictionary
{
    NSMutableDictionary *mappingDictionary = [NSMutableDictionary dictionary];
    
    [[self class] enumeratePropertiesIncludingSuperClasses:YES enumerator:^(objc_property_t property) 
    {
        NSDictionary *info = [[self class] informationForProperty:property];
        
        BOOL shouldSerialize =  [info[MHPropertyInfoShouldSerializeKey] boolValue];
        
        BOOL wantsToSerialize = [[self keysToSerialize] containsObject:info[MHPropertyInfoKeyNameKey]] || [[self keysToSerialize] containsObject:info[MHPropertyInfoPropertyNameKey]] || [[self keysToSerialize] containsObject:info[MHPropertyInfoIvarNameKey]];
        BOOL wantsToSkipSerialize = [[self keysToSkipSerialize] containsObject:info[MHPropertyInfoKeyNameKey]] || [[self keysToSkipSerialize] containsObject:info[MHPropertyInfoPropertyNameKey]] || [[self keysToSkipSerialize] containsObject:info[MHPropertyInfoIvarNameKey]];
        
        BOOL shouldProceed = (shouldSerialize && !wantsToSkipSerialize) || (!shouldSerialize && wantsToSerialize);
        
        if (shouldProceed) 
        {
            NSString *key = info[MHPropertyInfoIvarNameKey]?:info[MHPropertyInfoPropertyNameKey];
            
            id value = [self valueForKey:key];
            if (value) 
            {
                //if values is array and there is generic declared - calls the serializeToDictionary to its content
                if ([value isKindOfClass:[NSArray class]] && info[MHPropertyInfoGenericInfoKey][MHPropertyInfoClassNameKey]) 
                {
                    value = [value valueForKey:NSStringFromSelector(_cmd)];
                }
                
                [mappingDictionary setObject:value forKey:key];
            }
            else
            {
#warning should we handle nil values ?
            }
        }
    }];
    
    return [NSDictionary dictionaryWithDictionary:mappingDictionary];
}

#pragma mark - Mapping Info

+(MHObjectMappingAllocationBlock)mappingAllocationBlock
{
    MHObjectMappingAllocationBlock mappingAllocationBlock = ^id
    {
        return [[[self class] alloc] init]; 
    };
    
    return [mappingAllocationBlock copy];
}

-(Class)classForTargetObjectWithKey:(NSString *)key
{
    NSDictionary *info = [[self class] informationForTargetWithKey:key];
    Class cls = NSClassFromString(info[MHPropertyInfoClassNameKey]);
    return cls;
}

-(Class)classForElementsInTargetCollectionWithKey:(NSString *)key
{
    NSDictionary *info = [[self class] informationForTargetWithKey:key];
    Class cls = NSClassFromString(info[MHPropertyInfoGenericInfoKey][MHPropertyInfoClassNameKey]);
    return cls;
}

+(NSArray *)genericProtocols
{
    [NSException raise:NSInternalInconsistencyException format:@"You must override this method and returns an array of all generic protocols used."];
    return nil;
}

//static BOOL shouldReverseMappableCheck = NO;
static char shouldReverseMappableCheckKey;
+(BOOL)shouldReverseMappableCheck
{
    NSNumber *result = objc_getAssociatedObject(self, &shouldReverseMappableCheckKey)?:@NO;
    return [result boolValue];
//    return shouldReverseMappableCheck;
}

+(void)setShouldReverseMappableCheck:(BOOL)shouldReverse
{
    objc_setAssociatedObject(self, &shouldReverseMappableCheckKey, @(shouldReverse), OBJC_ASSOCIATION_COPY);
//    shouldReverseMappableCheck = shouldReverse;
}

//static BOOL shouldReverseSerializableCheck = NO;
static char shouldReverseSerializableCheckKey;
+(BOOL)shouldReverseSerializableCheck
{
    NSNumber *result = objc_getAssociatedObject(self, &shouldReverseSerializableCheckKey)?:@NO;
    return [result boolValue];
//    return shouldReverseSerializableCheck;
}

+(void)setShouldReverseSerializableCheck:(BOOL)shouldReverse
{
    objc_setAssociatedObject(self, &shouldReverseSerializableCheckKey, @(shouldReverse), OBJC_ASSOCIATION_COPY);
//    shouldReverseSerializableCheck = shouldReverse;
}

-(NSArray*)keysToMap
{
    return nil;
}

-(NSArray*)keysToSkipMap
{
    return nil;
}

-(NSArray*)keysToSerialize
{
    return nil;
}

-(NSArray*)keysToSkipSerialize
{
    return nil;
}

#pragma mark - Runtime

+(NSDictionary*)informationForTargetWithKey:(NSString*)key
{
    Class cls = [self class];
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setObject:key forKey:MHPropertyInfoKeyNameKey];
    
    objc_property_t __block property = class_getProperty(cls, [key UTF8String]);
    
    if (!property)
    {        
        [self enumeratePropertiesIncludingSuperClasses:YES enumerator:^(objc_property_t p) 
        {
            char *ivarNameChar =  property_copyAttributeValue(p, [MHPropertyIvarAttributeName UTF8String]);
            if (ivarNameChar) 
            {
                NSString *ivarName = [NSString stringWithCString:ivarNameChar encoding:NSUTF8StringEncoding];
                if ([ivarName isEqualToString:key]) 
                {
                    property = p;
                    [info setObject:ivarName forKey:MHPropertyInfoIvarNameKey];
                    return ;
                }
            }
        }];
    }
    
    NSDictionary *propertyInfo = [self informationForProperty:property];
    [info addEntriesFromDictionary:propertyInfo];
    
    return [NSDictionary dictionaryWithDictionary:info];
}

+(NSDictionary*)informationForProperty:(objc_property_t)property
{
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    
    if (property) 
    {
        //get ivar name
        char *ivarNameChar =  property_copyAttributeValue(property, [MHPropertyIvarAttributeName UTF8String]);
        if (ivarNameChar) 
        {
            NSString *ivarName = [NSString stringWithCString:ivarNameChar encoding:NSUTF8StringEncoding];
            [info setObject:ivarName forKey:MHPropertyInfoIvarNameKey];
        }
        
        //get property name
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        [info setObject:propertyName forKey:MHPropertyInfoPropertyNameKey];
        
        //get property type
        char *typeChar = property_copyAttributeValue(property, [MHPropertyTypeAttributeName UTF8String]);
        if (typeChar) 
        {
            NSString *type = [NSString stringWithCString:typeChar encoding:NSUTF8StringEncoding];
            if ([type hasPrefix:@"@"] && [type length] > 1)
            {
                //retrieve class name
                NSString * typeClassName = [type substringWithRange:NSMakeRange(2, [type length]-3)];  //turns @"NSString" into NSString
                
                //check for declared protocols
                NSRange declaredProtocolsRange = [typeClassName rangeOfString:@"<"];
                if (declaredProtocolsRange.location != NSNotFound) 
                {
                    //separate the declared protocols from the class name
                    NSString *declaredProtocols = [typeClassName substringFromIndex:declaredProtocolsRange.location];
                    typeClassName = [typeClassName substringToIndex:declaredProtocolsRange.location];
                    
                    declaredProtocols = [declaredProtocols substringFromIndex:1];
                    declaredProtocols = [declaredProtocols substringToIndex:declaredProtocols.length-1];
                    
                    //get the declared protocols and retieve their names
                    NSArray *declaredProtocolsNames = [declaredProtocols componentsSeparatedByString:@"><"];
                    [info setObject:declaredProtocolsNames forKey:MHPropertyInfoDeclaredProtocolsNamesKey];
                    
                    for (NSString *protocolName in declaredProtocolsNames) 
                    {                   
                        [self genericProtocols];
                        Protocol *protocol = objc_getProtocol([protocolName UTF8String]);
                        if (!protocol) 
                        {
                            [NSException raise:NSInternalInconsistencyException format:@"Protocol with name <%@> is not laoded. You must load it with @protocol() declaration", protocolName];
                        }
                        
                        //check if the protocol conforms to MHGeneric and get its genericType Class - used to declare type contained into array
                        if (protocol_conformsToProtocol(protocol, @protocol(MHGenerig))) 
                        {
                            objc_property_t genericTypeProperty = protocol_getProperty(protocol, [MHGenericTypePropertyName UTF8String], YES, YES);
                            NSDictionary *genericTypePropertyInfo = [self informationForProperty:genericTypeProperty];
                            [info setObject:genericTypePropertyInfo forKey:MHPropertyInfoGenericInfoKey];
                            break;
                        }
                    }
                }
                
                [info setObject:typeClassName forKey:MHPropertyInfoClassNameKey];
            }
        }
    }
    
    //set skip info
    BOOL isMarkedToMap = [info[MHPropertyInfoDeclaredProtocolsNamesKey] containsObject:NSStringFromProtocol(@protocol(MHMappable))];
    BOOL shouldReverseMappingCheck = [[self class] shouldReverseMappableCheck];
    BOOL shouldMap =  shouldReverseMappingCheck ? !isMarkedToMap : isMarkedToMap;
    [info setObject:@(shouldMap) forKey:MHPropertyInfoShouldMapKey];

    BOOL isMarkedToSerialize = [info[MHPropertyInfoDeclaredProtocolsNamesKey] containsObject:NSStringFromProtocol(@protocol(MHSerializable))];
    BOOL shouldReverseSerializeCheck = [[self class] shouldReverseSerializableCheck];
    BOOL shouldSerialize =  shouldReverseSerializeCheck ? !isMarkedToSerialize : isMarkedToSerialize;
    [info setObject:@(shouldSerialize) forKey:MHPropertyInfoShouldSerializeKey];
    
    return [NSDictionary dictionaryWithDictionary:info];
}

+(void)enumeratePropertiesIncludingSuperClasses:(BOOL)includeSuperClasses enumerator:(void(^)(objc_property_t property))enumerator
{
    Class cls = [self class];
    
    if (includeSuperClasses && cls && cls.superclass != [NSObject class]) 
    {
        [cls.superclass enumeratePropertiesIncludingSuperClasses:includeSuperClasses enumerator:enumerator];
    }
    
    unsigned int count = 0;
    objc_property_t *properties = class_copyPropertyList(cls, &count);
    for (int i=0; i<count; i++) 
    {
        objc_property_t property = properties[i];
        if (enumerator) 
        {
            enumerator(property);
        }
    }
    free(properties);
}

@end
