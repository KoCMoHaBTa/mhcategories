//
//  UIBarButtonItem+Blocks.m
//  BGMenu
//
//  Created by Milen Halachev on 10/23/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import "UIBarButtonItem+Blocks.h"
#import <objc/runtime.h>



@implementation UIBarButtonItem (Blocks)

@dynamic actionBlock;

static char actionBlockKey;

-(UIBarButtonItemActionBlock)actionBlock
{
    return objc_getAssociatedObject(self, &actionBlockKey);
}

-(void)setActionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    objc_setAssociatedObject(self, &actionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    if (self.target != self && self.action != @selector(forwardActionToBlock:))
    {
        self.target = self;
        self.action = @selector(forwardActionToBlock:);
    }
}

-(id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem actionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    self = [self initWithBarButtonSystemItem:systemItem target:self action:@selector(forwardActionToBlock:)];
    if (self)
    {
        self.actionBlock = actionBlock;
    }
    return self;
}

-(id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    self = [self initWithImage:image style:style target:self action:@selector(forwardActionToBlock:)];
    if (self)
    {
        self.actionBlock = actionBlock;
    }
    return self;
}

-(id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    self = [self initWithTitle:title style:style target:self action:@selector(forwardActionToBlock:)];
    if (self)
    {
        self.actionBlock = actionBlock;
    }
    return self;
}

-(void)forwardActionToBlock:(id)sender
{
    if (self.actionBlock)
    {
        self.actionBlock(sender);
    }
}

#pragma mark - class contructors

+(id)barButtonItemWithCustomView:(UIView *)customView
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:customView];
    
    return barButtonItem;
}

+(id)barButtonItemWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem target:(id)target action:(SEL)action
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem target:target action:action];
    
    return barButtonItem;
}

+(id)barButtonItemWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:style target:target action:action];
    
    return barButtonItem;
}

+(id)barButtonItemWithImage:(UIImage *)image landscapeImagePhone:(UIImage *)landscapeImagePhone style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[UIBarButtonItem alloc] initWithImage:image landscapeImagePhone:landscapeImagePhone style:style target:target action:action];
    
    return barButtonItem;
}

+(id)barButtonItemWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:style target:target action:action];
    
    return barButtonItem;
}

+(id)barButtonItemWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem actionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[[self class] alloc] initWithBarButtonSystemItem:systemItem actionBlock:actionBlock];
    
    return barButtonItem;
}

+(id)barButtonItemWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[[self class] alloc] initWithTitle:title style:style actionBlock:actionBlock];
    
    return barButtonItem;
}

+(id)barButtonItemWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock
{
    UIBarButtonItem *barButtonItem = nil;
    
    barButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:style actionBlock:actionBlock];
    
    return barButtonItem;
}


@end
