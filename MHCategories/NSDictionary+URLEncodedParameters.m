//
//  NSDictionary+URLEncodedParameters.m
//  MHCategories
//
//  Created by Milen Halachev on 5/30/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "NSDictionary+URLEncodedParameters.h"

@implementation NSDictionary (URLEncodedParameters)

-(NSString*)encodeString:(NSString*)string
{
    static NSString * const CharactersToBeEscaped = @":/?#[]@!$&'()*+,;="; //added reserved characters for encoding
    static NSString * const CharactersToLeaveUnescaped = NULL;
    
    return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef)CharactersToLeaveUnescaped, (__bridge CFStringRef)CharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
}

-(NSString *)URLEncodedParameters
{
    NSString * __block result = @"";
    
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) 
    {
        NSString *targetKey = [NSString stringWithFormat:@"%@", key];
        NSString *targetValue = [NSString stringWithFormat:@"%@", obj];
        
        targetKey = [self encodeString:targetKey];
        targetValue = [self encodeString:targetValue];
        
        NSString *targetPair = [NSString stringWithFormat:@"&%@=%@", targetKey, targetValue];
        result = [result stringByAppendingString:targetPair];
    }];
    
    result = [result substringFromIndex:1];
    return result;
}

@end
