//
//  NSMutableArray+Additions.h
//  Pizza
//
//  Created by Milen Halachev on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Additions)

-(NSString*)keyPathFromContainedStringKeys;

@end
