//
//  UIViewController+Storyboard.m
//  MHCategories
//
//  Created by Milen Halachev on 4/4/13.
//  Copyright (c) 2013 Milen Halachev. All rights reserved.
//

#import "UIViewController+Storyboard.h"

@implementation UIViewController (Storyboard)

+(id)controllerWithStoryboardName:(NSString *)storyboardName bundle:(NSBundle *)bundle identifier:(NSString *)identifier
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:bundle];
    id controller = [storyboard instantiateViewControllerWithIdentifier:identifier];
    return controller;
}

@end
