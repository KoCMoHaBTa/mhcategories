//
//  UIAlertView+Additions.m
//  BGMenu
//
//  Created by Milen Halachev on 10/8/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import "UIAlertView+Additions.h"
#import <objc/runtime.h>


@implementation UIAlertView (Additions)

+(UIAlertView *)alertViewWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:delegate
                                              cancelButtonTitle:cancelButtonTitle
                                              otherButtonTitles:nil];

    NSString *firstOtherButtonTitle = otherButtonTitles;
    NSString *anotherButtonTitle = nil;
    
    if (firstOtherButtonTitle)
    {
        [alertView addButtonWithTitle:firstOtherButtonTitle];
        
        va_list arguments;
        va_start(arguments, otherButtonTitles);
        
        while ((anotherButtonTitle = va_arg(arguments, id)))
        {
            [alertView addButtonWithTitle:anotherButtonTitle];
        }
        
        va_end(arguments);
    }
    
    
    
    return alertView;
}

+(void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:delegate
                                               cancelButtonTitle:cancelButtonTitle
                                               otherButtonTitles:nil];
    
    NSString *firstOtherButtonTitle = otherButtonTitles;
    NSString *anotherButtonTitle = nil;
    
    if (firstOtherButtonTitle)
    {
        [alertView addButtonWithTitle:firstOtherButtonTitle];
        
        va_list arguments;
        va_start(arguments, otherButtonTitles);
        
        while ((anotherButtonTitle = va_arg(arguments, id)))
        {
            [alertView addButtonWithTitle:anotherButtonTitle];
        }
        
        va_end(arguments);
    }

    [alertView show];
}

+(void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle
{
    [self showAlertViewWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
}

+(void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle
{
    [self showAlertViewWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
}

@end


@implementation UIAlertView (Blocks)

@dynamic buttonClickedBlock;
@dynamic shouldEnableFirstOtherButtonBlock;
@dynamic willPresentAlertViewBlock;
@dynamic didPresentAlertViewBlock;
@dynamic willDismissWithButtonIndexBlock;
@dynamic didDismissWithButtonIndexBlock;
@dynamic cancelBlock;
@dynamic didDismissWithButtonTypeBlock;
@dynamic didDismissWithButtonTypeAndCredentialBlock;
@dynamic didDismissWithButtonTypeAndInputBlock;

static char buttonClickedBlockKey;
static char shouldEnableFirstOtherButtonBlockKey;
static char willPresentAlertViewBlockKey;
static char didPresentAlertViewBlockKey;
static char willDismissWithButtonIndexBlockKey;
static char didDismissWithButtonIndexBlockKey;
static char cancelBlockKey;
static char didDismissWithButtonTypeBlockKey;
static char didDismissWithButtonTypeAndCredentialBlockKey;
static char didDismissWithButtonTypeAndInputBlockKey;

-(UIAlertViewButtonClickedBlock)buttonClickedBlock
{
    return objc_getAssociatedObject(self, &buttonClickedBlockKey);
}

-(void)setButtonClickedBlock:(UIAlertViewButtonClickedBlock)buttonClickedBlock
{
    objc_setAssociatedObject(self, &buttonClickedBlockKey, buttonClickedBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewShouldEnableFirstOtherButtonBlock)shouldEnableFirstOtherButtonBlock
{
    return objc_getAssociatedObject(self, &shouldEnableFirstOtherButtonBlockKey);
}

-(void)setShouldEnableFirstOtherButtonBlock:(UIAlertViewShouldEnableFirstOtherButtonBlock)shouldEnableFirstOtherButtonBlock
{
    objc_setAssociatedObject(self, &shouldEnableFirstOtherButtonBlockKey, shouldEnableFirstOtherButtonBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewWillPresentAlertViewBlock)willPresentAlertViewBlock
{
    return objc_getAssociatedObject(self, &willPresentAlertViewBlockKey);
}

-(void)setWillPresentAlertViewBlock:(UIAlertViewWillPresentAlertViewBlock)willPresentAlertViewBlock
{
    objc_setAssociatedObject(self, &willPresentAlertViewBlockKey, willPresentAlertViewBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewDidPresentAlertViewBlock)didPresentAlertViewBlock
{
    return objc_getAssociatedObject(self, &didPresentAlertViewBlockKey);
}

-(void)setDidPresentAlertViewBlock:(UIAlertViewDidPresentAlertViewBlock)didPresentAlertViewBlock
{
    objc_setAssociatedObject(self, &didPresentAlertViewBlockKey, didPresentAlertViewBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewWillDismissWithButtonIndexBlock)willDismissWithButtonIndexBlock
{
    return objc_getAssociatedObject(self, &willDismissWithButtonIndexBlockKey);
}

-(void)setWillDismissWithButtonIndexBlock:(UIAlertViewWillDismissWithButtonIndexBlock)willDismissWithButtonIndexBlock
{
    objc_setAssociatedObject(self, &willDismissWithButtonIndexBlockKey, willDismissWithButtonIndexBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewDidDismissWithButtonIndexBlock)didDismissWithButtonIndexBlock
{
    return objc_getAssociatedObject(self, &didDismissWithButtonIndexBlockKey);
}

-(void)setDidDismissWithButtonIndexBlock:(UIAlertViewDidDismissWithButtonIndexBlock)didDismissWithButtonIndexBlock
{
    objc_setAssociatedObject(self, &didDismissWithButtonIndexBlockKey, didDismissWithButtonIndexBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewCancelBlock)cancelBlock
{
    return objc_getAssociatedObject(self, &cancelBlockKey);
}

-(void)setCancelBlock:(UIAlertViewCancelBlock)cancelBlock
{
    objc_setAssociatedObject(self, &cancelBlockKey, cancelBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewButtonTypeBlock)didDismissWithButtonTypeBlock
{
    return objc_getAssociatedObject(self, &didDismissWithButtonTypeBlockKey);
}

-(void)setDidDismissWithButtonTypeBlock:(UIAlertViewButtonTypeBlock)didDismissWithButtonTypeBlock
{
    objc_setAssociatedObject(self, &didDismissWithButtonTypeBlockKey, didDismissWithButtonTypeBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewButtonTypeAndCredentialsBlock)didDismissWithButtonTypeAndCredentialBlock
{
    return objc_getAssociatedObject(self, &didDismissWithButtonTypeAndCredentialBlockKey);
}

-(void)setDidDismissWithButtonTypeAndCredentialBlock:(UIAlertViewButtonTypeAndCredentialsBlock)didDismissWithButtonTypeAndCredentialBlock
{
    objc_setAssociatedObject(self, &didDismissWithButtonTypeAndCredentialBlockKey, didDismissWithButtonTypeAndCredentialBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UIAlertViewButtonTypeAndInputBlock)didDismissWithButtonTypeAndInputBlock
{
    return objc_getAssociatedObject(self, &didDismissWithButtonTypeAndInputBlockKey);
}

-(void)setDidDismissWithButtonTypeAndInputBlock:(UIAlertViewButtonTypeAndInputBlock)didDismissWithButtonTypeAndInputBlock
{
    objc_setAssociatedObject(self, &didDismissWithButtonTypeAndInputBlockKey, didDismissWithButtonTypeAndInputBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.buttonClickedBlock)
    {
        self.buttonClickedBlock(alertView, buttonIndex);
    }
}

-(BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    if (self.shouldEnableFirstOtherButtonBlock)
    {
        return self.shouldEnableFirstOtherButtonBlock(alertView);
    }
    
    return YES;
}

-(void)willPresentAlertView:(UIAlertView *)alertView
{
    if (self.willPresentAlertViewBlock)
    {
        self.willPresentAlertViewBlock(alertView);
    }
}

-(void)didPresentAlertView:(UIAlertView *)alertView
{
    if (self.didPresentAlertViewBlock)
    {
        self.didPresentAlertViewBlock(alertView);
    }
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.willDismissWithButtonIndexBlock)
    {
        self.willDismissWithButtonIndexBlock(alertView, buttonIndex);
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.didDismissWithButtonIndexBlock)
    {
        self.didDismissWithButtonIndexBlock(alertView, buttonIndex);
    }
    
    if (self.didDismissWithButtonTypeBlock)
    {
        self.didDismissWithButtonTypeBlock(alertView, buttonIndex);
    }
    
    if (self.alertViewStyle == UIAlertViewStyleLoginAndPasswordInput && self.didDismissWithButtonTypeAndCredentialBlock)
    {
        UITextField *tfUsername = [alertView textFieldAtIndex:0];
        UITextField *tfPassword = [alertView textFieldAtIndex:1];
        
        NSString *username = tfUsername.text;
        NSString *password = tfPassword.text;
        
        self.didDismissWithButtonTypeAndCredentialBlock(alertView, buttonIndex, username, password);
    }
    
    if ((self.alertViewStyle == UIAlertViewStylePlainTextInput || self.alertViewStyle == UIAlertViewStyleSecureTextInput) && self.didDismissWithButtonTypeAndInputBlock) 
    {
        UITextField *inputTextField = [alertView textFieldAtIndex:0];
        
        NSString *input = inputTextField.text;
        
        self.didDismissWithButtonTypeAndInputBlock(alertView, buttonIndex, input);
    }
}

-(void)alertViewCancel:(UIAlertView *)alertView
{
    if (self.cancelBlock)
    {
        self.cancelBlock(alertView);
    }
}

#pragma mark - Class Methods

+(UIAlertView*)informalAlertViewWithTitle:(NSString *)title message:(NSString *)message dismissButtonTitle:(NSString *)dismissButtonTitle dismissBlock:(UIAlertViewButtonIndexBlock)dismissBlock
{
    UIAlertView *informalAlertView = [UIAlertView alertViewWithTitle:title message:message delegate:nil cancelButtonTitle:dismissButtonTitle otherButtonTitles: nil];
    
    informalAlertView.delegate = informalAlertView;
    [informalAlertView setDidDismissWithButtonIndexBlock:dismissBlock];
    
    return informalAlertView;
}

+(UIAlertView*)actionAlertViewWithTitle:(NSString *)title message:(NSString *)message positiveActionButtonTitle:(NSString *)positiveActionButtonTitle negativeActionButtonTitle:(NSString *)negativeActionButtonTitle dismissBlock:(UIAlertViewButtonTypeBlock)dismissBlock
{
    UIAlertView *actionAlertView = [UIAlertView alertViewWithTitle:title message:message delegate:nil cancelButtonTitle:negativeActionButtonTitle otherButtonTitles:positiveActionButtonTitle, nil];
    
    actionAlertView.delegate = actionAlertView;
    [actionAlertView setDidDismissWithButtonTypeBlock:dismissBlock];
    
    return actionAlertView;
}

+(UIAlertView*)alertViewWithTitle:(NSString*)title
                      message:(NSString*)message
                 dismissBlock:(UIAlertViewButtonIndexBlock)dismissBlock
            cancelButtonTitle:(NSString*)cancelButtonTitle
            otherButtonTitles:(NSString*)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                         message:message
                                                        delegate:nil
                                               cancelButtonTitle:cancelButtonTitle
                                               otherButtonTitles:nil];
    
    alertView.delegate = alertView;
    [alertView setDidDismissWithButtonIndexBlock:dismissBlock];
    
    NSString *firstOtherButtonTitle = otherButtonTitles;
    NSString *anotherButtonTitle = nil;
    
    if (firstOtherButtonTitle)
    {
        [alertView addButtonWithTitle:firstOtherButtonTitle];
        
        va_list arguments;
        va_start(arguments, otherButtonTitles);
        
        while ((anotherButtonTitle = va_arg(arguments, id)))
        {
            [alertView addButtonWithTitle:anotherButtonTitle];
        }
        
        va_end(arguments);
    }
    
//    for (UIView *subview in alertView.subviews)
//    {
//        if ([subview isKindOfClass:[UIButton class]])
//        {
//            UIButton *btn = (UIButton*)subview;
//            
//            CGSize s = [btn.titleLabel.text sizeWithFont:btn.titleLabel.font];
//            
//            if(s.width > btn.bounds.size.width)
//            {
//                btn.titleLabel.numberOfLines = 2;
//                btn.titleLabel.font = [UIFont fontWithName:btn.titleLabel.font.fontName size:8];
//            }
//        }
//    }
    
    return alertView;
}

+(UIAlertView*)loginAlertViewWithTitle:(NSString *)title message:(NSString *)message positiveActionButtonTitle:(NSString *)positiveActionButtonTitle negativeActionButtonTitle:(NSString *)negativeActionButtonTitle dismissBlock:(UIAlertViewButtonTypeAndCredentialsBlock)dismissBlock
{
    UIAlertView *loginAlertView = [UIAlertView alertViewWithTitle:title message:message delegate:nil cancelButtonTitle:negativeActionButtonTitle otherButtonTitles:positiveActionButtonTitle, nil];
    
    loginAlertView.delegate = loginAlertView;
    [loginAlertView setDidDismissWithButtonTypeAndCredentialBlock:dismissBlock];
    loginAlertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    
    return loginAlertView;
}

+(UIAlertView*)inputAlertViewWithTitle:(NSString *)title message:(NSString *)message positiveActionButtonTitle:(NSString *)positiveActionButtonTitle negativeActionButtonTitle:(NSString *)negativeActionButtonTitle secure:(BOOL)secure dismissBlock:(UIAlertViewButtonTypeAndInputBlock)dismissBlock
{
    UIAlertView *loginAlertView = [UIAlertView alertViewWithTitle:title message:message delegate:nil cancelButtonTitle:negativeActionButtonTitle otherButtonTitles:positiveActionButtonTitle, nil];
    
    loginAlertView.delegate = loginAlertView;
    [loginAlertView setDidDismissWithButtonTypeAndInputBlock:dismissBlock];
    loginAlertView.alertViewStyle = secure ? UIAlertViewStyleSecureTextInput : UIAlertViewStylePlainTextInput;
    
    return loginAlertView;
}

@end






