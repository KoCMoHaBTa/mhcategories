//
//  NSData+AESCrypt.h
//  Pizza
//
//  Created by Milen Halachev on 5/7/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

#define defaultSecret @"mtm"

@interface NSData (AESCrypt)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

+ (NSData *)dataWithBase64EncodedString:(NSString *)string;
- (id)initWithBase64EncodedString:(NSString *)string;

- (NSString *)base64Encoding;
- (NSString *)base64EncodingWithLineLength:(NSUInteger)lineLength;

- (BOOL)hasPrefixBytes:(const void *)prefix length:(NSUInteger)length;
- (BOOL)hasSuffixBytes:(const void *)suffix length:(NSUInteger)length;

+ (NSData*)dataWithObject:(id<NSCoding>)obj forKey:(NSString*)key;
- (id)objectForKey:(NSString*)key;

@end



@interface NSString (AESCrypt)

- (NSString *)AES256EncryptWithKey:(NSString *)key;
- (NSString *)AES256DecryptWithKey:(NSString *)key;

@end



@interface NSUserDefaults (AESCrypt)

-(void)setObject:(id<NSCoding>)value forKey:(NSString *)defaultName AES256EncryptWithSecret:(NSString*)secret;
-(id)objectForKey:(NSString *)defaultName AES256EncryptWithSecret:(NSString*)secret;

@end