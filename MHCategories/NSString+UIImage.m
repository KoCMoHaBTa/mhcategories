//
//  NSString+UIImage.m
//  Pizza
//
//  Created by Milen Halachev on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+UIImage.h"

@implementation NSString (UIImage)

-(UIImage *)imageRepresentationWithFont:(UIFont *)font color:(UIColor *)color
{
    CGSize size  = [self sizeWithFont:font];
    
    
    // check if UIGraphicsBeginImageContextWithOptions is available (iOS is 4.0+)
    if (UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    else
        // iOS is < 4.0 
        UIGraphicsBeginImageContext(size);
    
    // optional: add a shadow, to avoid clipping the shadow you should make the context size bigger 
    //
    // CGContextRef ctx = UIGraphicsGetCurrentContext();
    // CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [[UIColor grayColor] CGColor]);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    
    // draw in context, you can use also drawInRect:withFont:
    [self drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
//    CGRect rect = CGRectMake(0, 0, 10, 10);
//    [self drawInRect:rect withFont:font];
    
    
    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    
    
    return image;
}

-(UIImage *)imageRepresentationWithFont:(UIFont *)font color:(UIColor *)color toRect:(CGRect)rect
{
    return [self imageRepresentationWithFont:font color:color toRect:rect shadowColor:nil];
}

-(UIImage *)imageRepresentationWithFont:(UIFont *)font color:(UIColor *)color toRect:(CGRect)rect shadowColor:(UIColor*)shadowColor
{
    CGSize size  = rect.size;//[self sizeWithFont:font];
    
    
    // check if UIGraphicsBeginImageContextWithOptions is available (iOS is 4.0+)
    if (UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    else
        // iOS is < 4.0 
        UIGraphicsBeginImageContext(size);
    
    // optional: add a shadow, to avoid clipping the shadow you should make the context size bigger 
    //
    //    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //    CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [[UIColor grayColor] CGColor]);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    
    if (shadowColor) 
    {
        CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [shadowColor CGColor]);
    }
    
    // draw in context, you can use also drawInRect:withFont:
    //    [self drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
    //    CGRect rect = CGRectMake(0, 0, 10, 10);
    


    
    CGSize textSize = [self sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];//[self sizeWithFont:font forWidth:size.width lineBreakMode:UILineBreakModeWordWrap];
    CGFloat y = size.height/2 - textSize.height/2 ;

    
    [self drawInRect:CGRectMake(0, y, size.width, size.height) withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
        
    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    
    
    return image;
}

-(UIImage *)imageRepresentationWithFont:(UIFont *)font color:(UIColor *)color toRect:(CGRect)rect shadowColor:(UIColor *)shadowColor backgroundColor:(UIColor *)backgroundColor
{
    CGSize size  = rect.size;//[self sizeWithFont:font];
    
    
    // check if UIGraphicsBeginImageContextWithOptions is available (iOS is 4.0+)
    if (UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    else
        // iOS is < 4.0 
        UIGraphicsBeginImageContext(size);
    
    // optional: add a shadow, to avoid clipping the shadow you should make the context size bigger 
    //
    //    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //    CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [[UIColor grayColor] CGColor]);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    if (backgroundColor) 
    {
        CGContextSetFillColorWithColor(ctx, backgroundColor.CGColor);
//        CGContextSetAlpha(ctx, 0.5);
        CGContextFillRect(ctx, rect);
    }
    
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    
    if (shadowColor) 
    {
        CGContextSetShadowWithColor(ctx, CGSizeMake(1.0, 1.0), 5.0, [shadowColor CGColor]);
    }
    
    // draw in context, you can use also drawInRect:withFont:
    //    [self drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
    //    CGRect rect = CGRectMake(0, 0, 10, 10);
    
    
    
    
    CGSize textSize = [self sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];//[self sizeWithFont:font forWidth:size.width lineBreakMode:UILineBreakModeWordWrap];
    CGFloat y = size.height/2 - textSize.height/2 ;
    
    
    [self drawInRect:CGRectMake(0, y, size.width, size.height) withFont:font lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
    
    // transfer image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();    
    
    return image;
}

@end
