//
//  UIControl+Blocks.h
//  BGMenu
//
//  Created by Milen Halachev on 10/23/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^UIControlActionBlock)(id sender, id event);

@interface UIControl (Blocks)

-(void)addActionBlock:(UIControlActionBlock)actionBlock forEvents:(UIControlEvents)events UI_APPEARANCE_SELECTOR;
//-(NSArray*)actionBlocksForEvents:(UIControlEvents)events;
//-(void)removeActionBlocks:(NSArray*)actionBlocks;
-(void)removeAllActionBlocksForEvents:(UIControlEvents)events;


@end
