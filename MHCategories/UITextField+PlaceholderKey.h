//
//  UITextField+PlaceholderKey.h
//  BGMenu
//
//  Created by Milen Halachev on 1/15/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (PlaceholderKey)

@property(nonatomic, copy) NSString *placeholderKey;

@end
