//
//  NSDate+PRNumber.h
//  PhoneRace
//
//  Created by Milen Halachev on 9/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (NSDate_Additions)

- (NSNumber*) toPRNumber;
- (NSNumber*) year;
- (NSArray*) yearsSince1900;
- (NSString*) stringFormatOfDate;
- (NSString*) stringFormatOfTime;
- (NSString*) stringFormatOfTimeWithSeconds;
- (NSString*) stringFormatOfDateAndTime;
- (NSString*) stringFormatOfDateAndTimeWithTodayAndTomorrow;

- (NSDate*) dateByAddingDays:(NSNumber*)days;
- (NSDate*)dateByAddingMinutes:(NSNumber*)minutes;

- (NSInteger)minutes;
- (NSInteger)days;

- (NSDate*)dateByRoundingToMinutesInterval:(NSInteger)minutesInterval;
//- (NSDate*)dateByRoundingSecondsToZero;

@end
