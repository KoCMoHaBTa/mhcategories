//
//  NSObject+KVOBlocks.m
//  MHCategories
//
//  Created by Milen Halachev on 2/24/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "NSObject+KVOBlocks.h"
#import <objc/runtime.h>

@implementation NSObject (KVOBlocks)

-(KVOBlockObserver *)addObserverForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context block:(KVOBlock)block
{
    KVOBlockObserver *observer = [KVOBlockObserver observerWithObject:self forKeyPath:keyPath options:options context:context block:block];
    return observer;
}

-(void)removeBlockObserver:(KVOBlockObserver *)blockObserver
{
    [blockObserver invalidate];
}

@end

@interface KVOBlockObserver ()

@property(weak, readwrite) id object;
@property(copy, readwrite) NSString *keyPath;
@property(assign, readwrite) NSKeyValueObservingOptions options;
@property(assign, readwrite) void *context;
@property(copy, readwrite) KVOBlock block;

@end

@implementation KVOBlockObserver

+(instancetype)observerWithObject:(id)object forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context block:(KVOBlock)block
{
    KVOBlockObserver *observer = [[KVOBlockObserver alloc] init];
    observer.object = object;
    observer.keyPath = keyPath;
    observer.options = options;
    observer.context = context;
    observer.block = block;
    
    [object addObserver:observer forKeyPath:keyPath options:options context:context];
    
    return observer;
}

- (void)dealloc
{
    [self invalidate];
}

-(void)invalidate
{
    [self.object removeObserver:self forKeyPath:self.keyPath context:self.context];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (self.block) 
    {
        self.block(keyPath, object, change, context);
    }
}

@end