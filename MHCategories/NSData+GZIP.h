//
//  NSData+GZIP.h
//  FastMenuCore
//
//  Created by Milen Halachev on 1/23/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (GZIP)

- (NSData *)gzipDecompress;
- (NSData *)gzipCompress;

@end
