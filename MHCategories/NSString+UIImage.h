//
//  NSString+UIImage.h
//  Pizza
//
//  Created by Milen Halachev on 2/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUIBarButtonItemDefaultFont [UIFont fontWithName:@"Helvetica-Bold" size:12.0f]

@interface NSString (UIImage)

-(UIImage*)imageRepresentationWithFont:(UIFont*)font color:(UIColor*)color;
-(UIImage*)imageRepresentationWithFont:(UIFont*)font color:(UIColor*)color toRect:(CGRect)rect;
-(UIImage *)imageRepresentationWithFont:(UIFont *)font color:(UIColor *)color toRect:(CGRect)rect shadowColor:(UIColor*)shadowColor;
-(UIImage *)imageRepresentationWithFont:(UIFont *)font color:(UIColor *)color toRect:(CGRect)rect shadowColor:(UIColor*)shadowColor backgroundColor:(UIColor*)backgroundColor;

@end
