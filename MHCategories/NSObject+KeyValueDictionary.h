//
//  NSObject+KeyValueDictionary.h
//  FastMenuCore
//
//  Created by Milen Halachev on 7/11/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (KeyValueDictionary)

-(NSMutableDictionary *)keyValueDictionaryIncludingCollections:(BOOL)includeCollections;
-(NSMutableDictionary*)descpritionWithKeyValueDictionaryIncludingCollections:(BOOL)includeCollections;

@end

@interface NSArray (KeyValueDictionary)

-(NSArray *)descpritionWithKeyValueDictionaryIncludingCollections:(BOOL)includeCollections;

@end

@interface NSSet (KeyValueDictionary)

-(NSSet *)descpritionWithKeyValueDictionaryIncludingCollections:(BOOL)includeCollections;

@end