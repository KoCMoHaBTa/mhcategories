//
//  NSBundle+MHLocalization.h
//  MHCategories
//
//  Created by Milen Halachev on 6/7/13.
//  Copyright (c) 2013 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

//Notifications keys
#define MHLocalizableLoadTranslationsNotification @"MHLocalizableLoadTranslationsNotification"
#define MHLocalizableLanguageWillChangeNotification @"MHLocalizableLanguageWillChangeNotification"
#define MHLocalizableLanguageDidChangeNotification @"MHLocalizableLanguageDidChangeNotification"

//Notifications user info dictionary keys
#define MHLocalizableNotificationUserInfoDictionaryPrevousLanguageKey @"MHLocalizableNotificationUserInfoDictionaryPrevousLanguageKey"
#define MHLocalizableNotificationUserInfoDictionaryCurrentLanguageKey @"MHLocalizableNotificationUserInfoDictionaryCurrentLanguageKey"
#define MHLocalizableNotificationUserInfoDictionaryNewLanguageKey @"MHLocalizableNotificationUserInfoDictionaryNewLanguageKey"

@protocol MHLocalizable <NSObject>

@required

/*
 this method can be used to load one time translations
 notification will be posted when the language is changed in the runtime, so these translations can be reloaded
 */

-(void)loadTranslations:(NSNotification*)notification;


@optional

/*
 these methods will be called when language will/did change in runtime
 */

-(void)languageWillChange:(NSNotification*)notification;
-(void)languageDidChange:(NSNotification*)notification;

@end




@interface NSBundle (MHLocalization)

//this will be the default localization for all NSBundle instances
+(NSString*)currentLocalization;
+(void)setCurrentLocalization:(NSString*)currentLocalization;


//returns a bundle representing .lproj directory with the passed localization
-(NSBundle*)bundleForLocalization:(NSString*)localization;

//returns a bundle representing .lproj directory for the current localization
-(NSBundle*)bundleForCurrentLocalization;

//localizable objects registration & deregistration
+(void)registerLocalizableObject:(id<MHLocalizable>)localizableObject;
+(void)deregisterLocalizableObject:(id<MHLocalizable>)localizableObject;

//languages information
+(NSString*)systemLocalization;     //this is the current language in device settings
+(NSString*)systemLocale;           //this is the current locale in device settings

+(NSArray*)supportedSystemLocalizations;    //all languages in device settings
+(NSArray*)supportedSystemLocales;          //all locales in device settings

//-(NSString *)displayNameForLanguage:(NSString *)language translateToCurrentLanguage:(BOOL)translate;
+(NSString*)displayNameForLocalization:(NSString *)inputLocalization translatedToLocalization:(NSString*)traslationLocalization;

//@property(weak, nonatomic, readonly) NSArray *supportedDefaultBundleLanguages;
//@property(weak, nonatomic, readonly) NSArray *supportedMainBundleLanguages;
//@property(weak, nonatomic, readonly) NSArray *supportedNonBundleLanguages;

@end



