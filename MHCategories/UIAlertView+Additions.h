//
//  UIAlertView+Additions.h
//  BGMenu
//
//  Created by Milen Halachev on 10/8/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UIAlertView (Additions)

+(UIAlertView*)alertViewWithTitle:(NSString*)title
                          message:(NSString*)message
                         delegate:(id<UIAlertViewDelegate>)delegate
                cancelButtonTitle:(NSString*)cancelButtonTitle
                otherButtonTitles:(NSString*)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

+(void)showAlertViewWithTitle:(NSString*)title
                      message:(NSString*)message
                     delegate:(id<UIAlertViewDelegate>)delegate
            cancelButtonTitle:(NSString*)cancelButtonTitle
            otherButtonTitles:(NSString*)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

+(void)showAlertViewWithTitle:(NSString*)title
                      message:(NSString*)message
                     delegate:(id<UIAlertViewDelegate>)delegate
            cancelButtonTitle:(NSString*)cancelButtonTitle;

+(void)showAlertViewWithTitle:(NSString*)title
                      message:(NSString*)message
            cancelButtonTitle:(NSString*)cancelButtonTitle;

@end



//informal alert view button types
typedef NS_ENUM(NSInteger, UIActionAlertViewButtonType) 
{
    UIActionAlertViewButtonTypeNegative = 0,
    UIActionAlertViewButtonTypePositive = 1
};

//generic blocks
typedef void (^UIAlertViewButtonIndexBlock)(UIAlertView *alertView, NSInteger buttonIndex);
typedef void (^UIAlertViewBlock)(UIAlertView* alertView);

//delegate blocks
typedef UIAlertViewButtonIndexBlock UIAlertViewButtonClickedBlock;
typedef BOOL (^UIAlertViewShouldEnableFirstOtherButtonBlock)(UIAlertView *alertView);
typedef UIAlertViewBlock UIAlertViewWillPresentAlertViewBlock;
typedef UIAlertViewBlock UIAlertViewDidPresentAlertViewBlock;
typedef UIAlertViewButtonIndexBlock UIAlertViewWillDismissWithButtonIndexBlock;
typedef UIAlertViewButtonIndexBlock UIAlertViewDidDismissWithButtonIndexBlock;
typedef UIAlertViewBlock UIAlertViewCancelBlock;

//button type blocks
typedef void (^UIAlertViewButtonTypeBlock)(UIAlertView *alertView, UIActionAlertViewButtonType buttonType);
typedef void(^UIAlertViewButtonTypeAndCredentialsBlock)(UIAlertView *alertView, UIActionAlertViewButtonType buttonType, NSString *username, NSString *password);
typedef void(^UIAlertViewButtonTypeAndInputBlock)(UIAlertView *alertView, UIActionAlertViewButtonType buttonType, NSString *input);




@interface UIAlertView (Blocks) <UIAlertViewDelegate>

@property(nonatomic, copy) UIAlertViewButtonClickedBlock buttonClickedBlock;
@property(nonatomic, copy) UIAlertViewShouldEnableFirstOtherButtonBlock shouldEnableFirstOtherButtonBlock;
@property(nonatomic, copy) UIAlertViewWillPresentAlertViewBlock willPresentAlertViewBlock;
@property(nonatomic, copy) UIAlertViewDidPresentAlertViewBlock didPresentAlertViewBlock;
@property(nonatomic, copy) UIAlertViewWillDismissWithButtonIndexBlock willDismissWithButtonIndexBlock;
@property(nonatomic, copy) UIAlertViewDidDismissWithButtonIndexBlock didDismissWithButtonIndexBlock;
@property(nonatomic, copy) UIAlertViewCancelBlock cancelBlock;

@property(nonatomic, copy) UIAlertViewButtonTypeBlock didDismissWithButtonTypeBlock;
@property(nonatomic, copy) UIAlertViewButtonTypeAndCredentialsBlock didDismissWithButtonTypeAndCredentialBlock;
@property(nonatomic, copy) UIAlertViewButtonTypeAndInputBlock didDismissWithButtonTypeAndInputBlock;


+(UIAlertView*)informalAlertViewWithTitle:(NSString*)title
                              message:(NSString*)message
                   dismissButtonTitle:(NSString*)dismissButtonTitle
                         dismissBlock:(UIAlertViewButtonIndexBlock)dismissBlock;

+(UIAlertView*)actionAlertViewWithTitle:(NSString*)title
                            message:(NSString*)message
          positiveActionButtonTitle:(NSString*)positiveActionButtonTitle
          negativeActionButtonTitle:(NSString*)negativeActionButtonTitle
                       dismissBlock:(UIAlertViewButtonTypeBlock)dismissBlock;

+(UIAlertView*)alertViewWithTitle:(NSString*)title
                      message:(NSString*)message
                 dismissBlock:(UIAlertViewButtonIndexBlock)dismissBlock
            cancelButtonTitle:(NSString*)cancelButtonTitle
            otherButtonTitles:(NSString*)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

+(UIAlertView*)loginAlertViewWithTitle:(NSString*)title
                           message:(NSString*)message
         positiveActionButtonTitle:(NSString*)positiveActionButtonTitle
         negativeActionButtonTitle:(NSString*)negativeActionButtonTitle
                      dismissBlock:(UIAlertViewButtonTypeAndCredentialsBlock)dismissBlock;

+(UIAlertView*)inputAlertViewWithTitle:(NSString*)title
                           message:(NSString*)message
         positiveActionButtonTitle:(NSString*)positiveActionButtonTitle
         negativeActionButtonTitle:(NSString*)negativeActionButtonTitle
                            secure:(BOOL)secure
                      dismissBlock:(UIAlertViewButtonTypeAndInputBlock)dismissBlock;


-(void)setButtonClickedBlock:(UIAlertViewButtonClickedBlock)buttonClickedBlock;
-(void)setShouldEnableFirstOtherButtonBlock:(UIAlertViewShouldEnableFirstOtherButtonBlock)shouldEnableFirstOtherButtonBlock;
-(void)setWillPresentAlertViewBlock:(UIAlertViewWillPresentAlertViewBlock)willPresentAlertViewBlock;
-(void)setDidPresentAlertViewBlock:(UIAlertViewDidPresentAlertViewBlock)didPresentAlertViewBlock;
-(void)setWillDismissWithButtonIndexBlock:(UIAlertViewWillDismissWithButtonIndexBlock)willDismissWithButtonIndexBlock;
-(void)setDidDismissWithButtonIndexBlock:(UIAlertViewDidDismissWithButtonIndexBlock)didDismissWithButtonIndexBlock;
-(void)setCancelBlock:(UIAlertViewCancelBlock)cancelBlock;

-(void)setDidDismissWithButtonTypeBlock:(UIAlertViewButtonTypeBlock)didDismissWithButtonTypeBlock;
-(void)setDidDismissWithButtonTypeAndCredentialBlock:(UIAlertViewButtonTypeAndCredentialsBlock)didDismissWithButtonTypeAndCredentialBlock;
-(void)setDidDismissWithButtonTypeAndInputBlock:(UIAlertViewButtonTypeAndInputBlock)didDismissWithButtonTypeAndInputBlock;


@end