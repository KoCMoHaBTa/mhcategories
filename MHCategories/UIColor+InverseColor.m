//
//  UIColor+InverseColor.m
//  Pizza
//
//  Created by Milen Halachev on 5/31/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import "UIColor+InverseColor.h"

@implementation UIColor (InverseColor)

- (UIColor *) inverseColor 
{
    
    CGColorRef oldCGColor = self.CGColor;
    
    int numberOfComponents = (int)CGColorGetNumberOfComponents(oldCGColor);
    
    // can not invert - the only component is the alpha
    // e.g. self == [UIColor groupTableViewBackgroundColor]
    if (numberOfComponents == 1) {
        return [UIColor colorWithCGColor:oldCGColor];
    }
    
    const CGFloat *oldComponentColors = CGColorGetComponents(oldCGColor);
    CGFloat newComponentColors[numberOfComponents];
    
    int i = numberOfComponents - 1;
    newComponentColors[i] = oldComponentColors[i]; // alpha
    while (--i >= 0) {
        newComponentColors[i] = 1 - oldComponentColors[i];
    }
    
    CGColorRef newCGColor = CGColorCreate(CGColorGetColorSpace(oldCGColor), newComponentColors);
    UIColor *newColor = [UIColor colorWithCGColor:newCGColor];
    CGColorRelease(newCGColor);
    
    return newColor;
}

-(float)getBrightness
{
//    If it is > 0.5 it is bright, and otherwise dark.
    const CGFloat *componentColors = CGColorGetComponents(self.CGColor);
    
    
    float red;
    float green;
    float blue;
    
    red = componentColors[0];
    green = componentColors[1];
    blue = componentColors[2];
    
    float num = red;// / 255.0f;
    float num2 = blue;// / 255.0f;
    float num3 = green;// / 255.0f;
    float num4 = num;
    float num5 = num;
    if (num2 > num4)
        num4 = num2;
    if (num3 > num4)
        num4 = num3;
    if (num2 < num5)
        num5 = num2;
    if (num3 < num5)
        num5 = num3;
    return ((num4 + num5) / 2.0f);
}


@end
