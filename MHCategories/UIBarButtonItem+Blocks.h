//
//  UIBarButtonItem+Blocks.h
//  BGMenu
//
//  Created by Milen Halachev on 10/23/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^UIBarButtonItemActionBlock)(id sender);

@interface UIBarButtonItem (Blocks)

@property(nonatomic, copy) UIBarButtonItemActionBlock actionBlock;

-(id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem actionBlock:(UIBarButtonItemActionBlock)actionBlock;
-(id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock;
-(id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock;

-(void)forwardActionToBlock:(id)sender;

+(id)barButtonItemWithCustomView:(UIView *)customView;
+(id)barButtonItemWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem target:(id)target action:(SEL)action;
+(id)barButtonItemWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action;
+(id)barButtonItemWithImage:(UIImage *)image landscapeImagePhone:(UIImage *)landscapeImagePhone style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action;
+(id)barButtonItemWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action;
+(id)barButtonItemWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem actionBlock:(UIBarButtonItemActionBlock)actionBlock;
+(id)barButtonItemWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock;
+(id)barButtonItemWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style actionBlock:(UIBarButtonItemActionBlock)actionBlock;

@end
