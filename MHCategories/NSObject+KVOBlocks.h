//
//  NSObject+KVOBlocks.h
//  MHCategories
//
//  Created by Milen Halachev on 2/24/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KVOBlockObserver;
typedef void(^KVOBlock)(NSString *keypath, id object, NSDictionary *change, void *context);

@interface NSObject (KVOBlocks)

-(KVOBlockObserver*)addObserverForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context block:(KVOBlock)block;
-(void)removeBlockObserver:(KVOBlockObserver*)blockObserver; //this calls -ivalidate on the receiver

@end

@interface KVOBlockObserver : NSObject
{
    
}

@property(weak, readonly) id object;
@property(copy, readonly) NSString *keyPath;
@property(assign, readonly) NSKeyValueObservingOptions options;
@property(assign, readonly) void *context;
@property(copy, readonly) KVOBlock block;

+(instancetype)observerWithObject:(id)object forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context block:(KVOBlock)block;
-(void)invalidate;

@end