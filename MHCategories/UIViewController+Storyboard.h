//
//  UIViewController+Storyboard.h
//  MHCategories
//
//  Created by Milen Halachev on 4/4/13.
//  Copyright (c) 2013 Milen Halachev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Storyboard)

+(id)controllerWithStoryboardName:(NSString*)storyboardName bundle:(NSBundle*)bundle identifier:(NSString*)identifier;

@end
