//
//  NSObject+SubclassDiscovery.m
//  FastMenuCore
//
//  Created by Milen Halachev on 11/27/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import "NSObject+SubclassDiscovery.h"
#import <objc/runtime.h>

@implementation NSObject (SubclassDiscovery)

+ (NSArray *) allSubclasses
{    
    Class myClass = [self class];
    NSMutableArray *mySubclasses = [NSMutableArray array];
    
    unsigned int numOfClasses;
    Class *classes = objc_copyClassList(&numOfClasses);
    for (unsigned int ci = 0; ci < numOfClasses; ci++) {
        // Replace the code in this loop to limit the result to immediate subclasses:
        // Class superClass = class_getSuperclass(classes[ci]);
        // if (superClass == myClass)
        //  [mySubclasses addObject: classes[ci]];
        Class superClass = classes[ci];
        do {
            superClass = class_getSuperclass(superClass);
        } while (superClass && superClass != myClass);
        
        if (superClass)
            [mySubclasses addObject: classes[ci]];
    }
    free(classes);
        
    return mySubclasses;
}

+ (NSArray *)allDirectSubclasses
{
    Class myClass = [self class];
    
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings)
    {
        Class evaluatedClass = evaluatedObject;
        
        return [evaluatedClass superclass] == myClass;
    }];
    
    NSArray *allDirectSubclasses = [[self allSubclasses] filteredArrayUsingPredicate:predicate];
    
    return allDirectSubclasses;
}

+(NSArray *)allIvarKeys
{
    NSMutableArray *ivarKeys = [NSMutableArray array];
    
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    for (int i=0; i<count; i++) 
    {
        Ivar ivar = ivars[i];
        const char *name = ivar_getName(ivar);
        [ivarKeys addObject:[NSString stringWithUTF8String:name]];
    }
    
    return ivarKeys;
}


@end
