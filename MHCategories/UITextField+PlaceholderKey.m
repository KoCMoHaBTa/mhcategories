//
//  UITextField+PlaceholderKey.m
//  BGMenu
//
//  Created by Milen Halachev on 1/15/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import "UITextField+PlaceholderKey.h"

@implementation UITextField (PlaceholderKey)

@dynamic placeholderKey;

static char placeholderKeyKey;

-(NSString *)placeholderKey
{
    return objc_getAssociatedObject(self, &placeholderKeyKey);
}

-(void)setPlaceholderKey:(NSString *)placeholderKey
{
    objc_setAssociatedObject(self, &placeholderKeyKey, placeholderKey, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
