//
//  UIView+Additions.m
//  House Doctor
//
//  Created by Milen Halachev on 12/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "UIView+Additions.h"
#import <QuartzCore/QuartzCore.h>


static CGFloat defaultBorderWidth;
static CGColorRef defaultBorderColor;
static CGFloat defaultCornerRadius;

static BOOL areDefaultValuesSet = NO;

@implementation UIView (UIView_Additions)

-(void) setRoundedCornersWithRadius:(CGFloat)radius borderColor:(CGColorRef)cgColor borderWidth:(CGFloat)borderWidth
{
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor = cgColor;
    self.layer.cornerRadius = radius;
}

-(void) setRoundedCorners:(BOOL)rounded
{
    
    if (areDefaultValuesSet == NO) 
    {
        defaultBorderWidth = self.layer.borderWidth;
        defaultBorderColor = self.layer.borderColor;
        defaultCornerRadius = self.layer.cornerRadius;
        
        areDefaultValuesSet = YES;
    }
    
    if (rounded) 
    {        
        [self setRoundedCornersWithRadius:8 borderColor:[[UIColor grayColor] CGColor] borderWidth:1];
        
    }
    else
    {        
        [self setRoundedCornersWithRadius:defaultCornerRadius borderColor:defaultBorderColor borderWidth:defaultBorderWidth];
    }
}

@end
