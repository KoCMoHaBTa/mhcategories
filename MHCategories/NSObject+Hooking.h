//
//  NSObject+Hooking.h
//  FastMenu
//
//  Created by Milen Halachev on 11/5/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^NSObjectMethodHookBlock)(void);

@interface NSObject (Hooking)

+(void)hookSourceClass:(Class)sourceCls sourceMethod:(SEL)sourceMethod destinationClass:(Class)destinationClass destinationMethod:(SEL)destinationMethod;
//+(void)hookSourceClass:(Class)sourceCls sourceMethod:(SEL)sourceMethod block:(id)block; //method_return_type ^(id self, self, method_args …)


    
@end
