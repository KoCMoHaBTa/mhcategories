//
//  NSManagedObject+Copy.h
//  Pizza
//
//  Created by Milen Halachev on 4/21/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

//
//  NSManagedObject+Clone.h
//

#import <CoreData/CoreData.h>

#ifndef CD_CUSTOM_DEBUG_LOG
#define CD_CUSTOM_DEBUG_LOG NSLog
#endif

@interface NSManagedObject (Copy)

- (NSManagedObject *)cloneInContext:(NSManagedObjectContext *)context
                    excludeEntities:(NSArray *)namesOfEntitiesToExclude;

@end
