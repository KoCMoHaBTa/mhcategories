//
//  NSJSONSerialization+NSNullRemoval.m
//  MHCategories
//
//  Created by Milen Halachev on 7/3/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "NSJSONSerialization+NSNullRemoval.h"

@implementation NSJSONSerialization (NSNullRemoval)

+(id)JSONObjectWithData:(NSData *)data options:(NSJSONReadingOptions)opt error:(NSError *__autoreleasing *)error removeNSNull:(BOOL)removeNSNull
{
    if (removeNSNull)
    {
        opt = opt | NSJSONReadingMutableContainers;
        opt = opt | NSJSONReadingMutableLeaves;
    }
    
    id result = [self JSONObjectWithData:data options:opt error:error];
    
    if (removeNSNull) 
    {
        if ([result isEqual:[NSNull null]]) 
        {
            return nil;
        }
        
        if ([result isKindOfClass:[NSMutableArray class]] || [result isKindOfClass:[NSMutableDictionary class]]) 
        {
            [result removeNSNull];
        }
    }
    
    return result;
}

@end


@implementation NSMutableArray (NSNullRemoval)

-(void)removeNSNull
{
    NSMutableIndexSet *nullIndexes = [NSMutableIndexSet indexSet];
    
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) 
    {
        if([obj isEqual:[NSNull null]])
        {
            [nullIndexes addIndex:idx];
            return;
        }
        
        if ([obj isKindOfClass:[NSMutableArray class]] || [obj isKindOfClass:[NSMutableDictionary class]]) 
        {
            [obj removeNSNull];
            return;
        }
    }];
        
    [self removeObjectsAtIndexes:nullIndexes];
}

@end


@implementation NSMutableDictionary (NSNullRemoval)

-(void)removeNSNull
{
    NSMutableArray *nullKeys = [NSMutableArray array];
    
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) 
    {
        if([obj isEqual:[NSNull null]])
        {
            [nullKeys addObject:key];
            return;
        }
        
        if ([obj isKindOfClass:[NSMutableArray class]] || [obj isKindOfClass:[NSMutableDictionary class]]) 
        {
            [obj removeNSNull];
            return;
        }
    }];
    
    [self removeObjectsForKeys:nullKeys];
}

@end