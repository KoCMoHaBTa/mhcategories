//
//  NSMutableArray+Additions.m
//  Pizza
//
//  Created by Milen Halachev on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSMutableArray+Additions.h"

@implementation NSMutableArray (Additions)

-(NSString*)keyPathFromContainedStringKeys
{
    NSString *keyPath = @"";
    
    for (NSString *key in self) 
    {
        keyPath = [keyPath stringByAppendingFormat:@".%@", key];
    }
    
    return keyPath;
}

@end
