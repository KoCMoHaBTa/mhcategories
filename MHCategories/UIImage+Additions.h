//
//  UIImage+Additions.h
//  BGMenu
//
//  Created by Milen Halachev on 10/8/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

+(UIImage*)imageWithImages:(UIImage*)images, ... NS_REQUIRES_NIL_TERMINATION;
-(void)addImage:(UIImage*)image;

@end
