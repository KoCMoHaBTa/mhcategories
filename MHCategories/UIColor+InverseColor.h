//
//  UIColor+InverseColor.h
//  Pizza
//
//  Created by Milen Halachev on 5/31/12.
//  Copyright (c) 2012 Plovdiv Univercity / Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (InverseColor)

- (UIColor *) inverseColor;
-(float)getBrightness;


@end
