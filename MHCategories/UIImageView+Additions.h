//
//  UIImageView+Additions.h
//  BGMenu
//
//  Created by Milen Halachev on 10/9/12.
//  Copyright (c) 2012 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Additions)

-(void)setShadowBorderWithColor:(UIColor*)shadowColor
                    borderColor:(UIColor*)borderColor
                    borderWidth:(CGFloat)borderWidth
                   cornerRadius:(CGFloat)cornerRadius
                   shadowRadius:(CGFloat)shadowRadius;

-(void)setDefaultBorderShadow;

@end
