//
//  NSJSONSerialization+NSNullRemoval.h
//  MHCategories
//
//  Created by Milen Halachev on 7/3/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSJSONSerialization (NSNullRemoval)

+(id)JSONObjectWithData:(NSData *)data options:(NSJSONReadingOptions)opt error:(NSError *__autoreleasing *)error removeNSNull:(BOOL)removeNSNull;

@end


@interface NSMutableArray (NSNullRemoval)

-(void)removeNSNull;

@end


@interface NSMutableDictionary (NSNullRemoval)

-(void)removeNSNull;

@end