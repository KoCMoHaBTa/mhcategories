//
//  NSString+Validation.m
//  PhoneRace
//
//  Created by Milen Halachev on 10/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSString+Validation.h"


//NSString* const EMPTY_STRING = @"";
#define EMPTY_STRING @""


@implementation NSString (NSString_Validation)

-(BOOL)isValidEmailAddressUsingStricterFilter:(BOOL)strict
{
    // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    
    if ([self isEqualToEmptyString]) 
    {
        return NO;
    }
    
    BOOL stricterFilter = strict;
    
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
    
}

-(BOOL)isEqualToEmptyString
{
    if ([self isEqualToString:EMPTY_STRING]) 
    {
        return YES;
    }
    else 
    {
        return NO;
    }
}

-(NSString *)firstChar
{
    return [self substringToIndex:1];
}

@end
