//
//  UIView+DimensionAccessors.h
//  BGMenu
//
//  Created by Milen Halachev on 2/28/13.
//  Copyright (c) 2013 Web Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DimensionAccessors)

@property(nonatomic, assign) CGPoint frameOrigin;
@property(nonatomic, assign) CGFloat frameX;
@property(nonatomic, assign) CGFloat frameY;

@property(nonatomic, assign) CGSize frameSize;
@property(nonatomic, assign) CGFloat frameWidth;
@property(nonatomic, assign) CGFloat frameHeight;

//@property(nonatomic, assign) CGFloat boundsOrigin;
//@property(nonatomic, assign) CGFloat boundsX;
//@property(nonatomic, assign) CGFloat boundsY;
//
//@property(nonatomic, assign) CGFloat boundsSize;
//@property(nonatomic, assign) CGFloat boundsWidth;
//@property(nonatomic, assign) CGFloat boundsHeight;

-(void)resizeToFitSubviews;

@end
