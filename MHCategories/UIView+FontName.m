//
//  NSObject+FontName.m
//  MHCategories
//
//  Created by Milen Halachev on 5/14/14.
//  Copyright (c) 2014 Milen Halachev. All rights reserved.
//

#import "UIView+FontName.h"
#import <objc/runtime.h>

@implementation UIView (FontName)

@dynamic fontName;
@dynamic fontKey;
@dynamic font;

static NSMutableDictionary *__fontMapping = nil;
static NSMutableDictionary *__excludeContainers = nil;

+(void)registerFontName:(NSString *)fontName forKey:(NSString *)fontKey symbolicTraits:(UIFontDescriptorSymbolicTraits)symbolicTraits
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        __fontMapping = [NSMutableDictionary dictionary];
    });
    
    NSMutableDictionary *traitMapping = [__fontMapping[fontKey] mutableCopy]?:[@{} mutableCopy];
    [traitMapping setObject:fontName forKey:@(symbolicTraits)];
    [__fontMapping setObject:traitMapping forKey:fontKey];
}

+(void)excludeFontAppearanceFromContainer:(Class)container
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
    {
        __excludeContainers = [NSMutableDictionary dictionary];
    });
    
    NSMutableArray *containers = [__excludeContainers[NSStringFromClass([self class])] mutableCopy]?:[@[] mutableCopy];
    [containers addObject:NSStringFromClass(container)];
    [__excludeContainers setObject:containers forKey:NSStringFromClass([self class])];
}

-(NSString *)fontKey
{
    NSString *fontName = self.fontName;
    NSSet *keys = [__fontMapping keysOfEntriesPassingTest:^BOOL(NSString *key, NSDictionary *obj, BOOL *stop) 
                   {
                       NSSet *traitKeys = [obj keysOfEntriesPassingTest:^BOOL(NSNumber *key, NSString *obj, BOOL *stop) 
                                           {
                                               BOOL result = [obj isEqualToString:fontName];
                                               //            *stop = result;
                                               return result;
                                               
                                           }];
                       
                       NSNumber *traitKey = [traitKeys anyObject];
                       
                       BOOL result = traitKey ? YES : NO;
                       //        *stop = result;
                       return result;
                       
                   }];
    
    NSString *key = [keys anyObject];
    
    return key;
}

-(void)setFontKey:(NSString *)fontKey
{
    UIFontDescriptorSymbolicTraits currentTraits = self.font.fontDescriptor.symbolicTraits;
//    NSString *currentFontName = self.fontName;
    
    NSDictionary *traitMapping = __fontMapping[fontKey];
//    BOOL allreadySet = [[traitMapping keysOfEntriesPassingTest:^BOOL(id key, id obj, BOOL *stop) 
//    {
//        BOOL result = [currentFontName isEqualToString:obj];
//        *stop = result;
//        return result;
//        
//    }] count] == 1;
//    
//    if (allreadySet) 
//    {
//        return;
//    }
    
    NSSet *keys = [traitMapping keysOfEntriesPassingTest:^BOOL(NSNumber *key, NSString *obj, BOOL *stop) 
                   {        
                       UIFontDescriptorSymbolicTraits traits = [key unsignedIntValue];
                       if (currentTraits == traits) 
                       {
                           *stop = YES;
                           return YES;
                       }
                       
                       BOOL result = ((currentTraits & traits) == traits);
//                       BOOL result = ((traits != 0) && ((currentTraits & traits) == traits));
                       return result;
                       
                   }];
    
    
    
    
    
    NSNumber *key = [keys valueForKeyPath:@"@max.unsignedIntegerValue"];
//    if (keys.count > 1) 
//    {
//        NSLog(@"Multiple Fonts Matched:");
//        NSLog(@"Matched Traits: %@", keys);
//        NSLog(@"Current Traits: %@", @(currentTraits));
//        NSLog(@"Font Name: %@", self.fontName);
//        NSLog(@"Font Key: %@", fontKey);
//        NSLog(@"Selected: %@", key);
//    }
    
    NSString *fontName = traitMapping[key];
//    NSString *fontName = traitMapping[key]?:traitMapping[@0];
    
    [self setFontName:fontName];
}

-(NSString *)fontName
{
    return self.font.fontName;
}

-(void)setFontName:(NSString *)fontName
{        
    NSArray *excludeContainers = __excludeContainers[NSStringFromClass([self class])];
    if (excludeContainers.count > 0) 
    {
        UIView *superview = self.superview;
        while (superview) 
        {
            if ([excludeContainers containsObject:NSStringFromClass([superview class])]) 
            {
                return;
            }
            
            superview = superview.superview;
        }
    }
    
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end












